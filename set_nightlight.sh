#! /bin/bash

######################## CONSTANTS ########################

# The most nightlight is represented by the lowest numeric value.
readonly MAX_NIGHTLIGHT=1700

# The least nightlight is represented by the highest numeric value.
readonly MIN_NIGHTLIGHT=4700

readonly GSETTINGS_NIGHTLIGHT_KEY="org.gnome.settings-daemon.plugins.color night-light-temperature"

# Exit codes
readonly EXIT_SUCCESS=0
readonly EXIT_INVALID_ARG=1

# Used in function log()
# https://en.wikipedia.org/wiki/ANSI_escape_code#Colors
readonly TEXT_STYLE_END="\x1B[0m"
readonly TEXT_STYLE_FG_ERROR="\x1B[38;5;196m"
readonly TEXT_STYLE_FG_WARNING="\x1B[4;38;5;208m"
readonly TEXT_STYLE_FG_INFO="\x1B[38;5;46m"
readonly TEXT_STYLE_FG_DEBUG="\x1B[38;5;39m"
readonly TEXT_STYLE_FG_VERBOSE="\x1B[2;37m"

######################## GLOBALS ########################

# Used in function log()
g_log_level=1
g_log_enable_ansi=0
g_log_enable_timestamps=0
g_log_use_precise_timestamps=0
g_log_use_short_tags=0
g_log_sink="1"
g_log_sink_is_fd=1
g_log_error_sink="2"
g_log_error_sink_is_fd=1

######################## FUNCTIONS ########################

# Print a log message.
#
# This is version 1 of the log function.
#
# PARAMETERS
#
# a) For levels verbose, debug, info and warning:
#   1. The log level of the message.
#   2. A prefix for the message.
#   3+. The messages that should be printed to $g_log_sink.
#
# b) For level error:
#   1. The log level of the message.
#   2. A prefix for the message.
#   3. Exit code for the script.
#   4. Extra code to evaluate before exiting.
#   5+. The messages that should be printed to $g_log_error_sink.
#
# LEVELS
#
#   verbose -> 1
#   debug -> 2
#   info -> 3
#   warning -> 4
#   error -> 5 or greater
#
# GLOBALS
#
# $g_log_level:
#   Controls which events will be written to stdout, and which
#   will be skipped.
#
# $g_log_enable_ansi:
#   Controls whether ANSI coloring will be enabled for the log messages.
#
# $g_log_enable_timestamps:
#   Controls whether the log messages include timestamps. The timestamps
#   are printed in the ISO 8601 format.
#
# $g_log_use_precise_timestamps:
#   Controls the precision of the timestamps.
#
#   Has no effect, if $g_log_enable_timestamps is 0.
#
# $g_log_use_short_tags:
#   Controls whether the tags for the level of the log message should be
#   short (a single letter) or long (the full word for the level).
#
# $g_log_sink:
#   The sink to which all log messages, except error messages, are written to.
#   Can be a file or a file descriptor. If the sink is a file then the messages
#   will be appended to an existing file, otherwise a new one will be created
#   for the given name. If the sink is a file descriptor, then
#   $g_log_sink_is_fd must be set to 1, otherwise the file descriptor number
#   will be interpreted as a filename.
#
# $g_log_sink_is_fd:
#   Whether or not the value for $g_log_sink should be interpreted as a file
#   descriptor or as a filepath.
#
# $g_log_error_sink:
#   This is effectively the same as $g_log_sink, except only for error log
#   messages.
#
# $g_log_error_sink_is_fd:
#   Whether or not the value for $g_log_error_sink should be interpreted as a
#   file descriptor or as a filepath.
#
# EXAMPLE
#
#   log 1 " " "My verbose message with a tab prefix"
#
#   log 3 "" "My info message"
#
#   log 5 "" 24 "" "My error message with exit code 24"
#
#   log 5 "" 24 "ls -1" \
#     "My error message with exit code 24 and" \
#     "a list of all files in the current directory"
function log() {
  local log_level="$1"
  if [ "$g_log_level" -gt "$log_level" ]; then
    return
  fi

  local prefix="$2"

  local tag
  local style_start
  local style_end="$TEXT_STYLE_END"

  if [ "$log_level" -eq 1 ]; then # verbose
    tag="VERBOSE"
    style_start="$TEXT_STYLE_FG_VERBOSE"
  elif [ "$log_level" -eq 2 ]; then # debug
    tag="DEBUG"
    style_start="$TEXT_STYLE_FG_DEBUG"
  elif [ "$log_level" -eq 3 ]; then # info
    tag="INFO"
    style_start="$TEXT_STYLE_FG_INFO"
  elif [ "$log_level" -eq 4 ]; then # warning
    tag="WARNING"
    style_start="$TEXT_STYLE_FG_WARNING"
  elif [ "$log_level" -ge 5 ]; then # error
    tag="ERROR"
    style_start="$TEXT_STYLE_FG_ERROR"
  fi

  if [ 0 -ne "$g_log_use_short_tags" ]; then
    tag="${tag:0:1}"
  fi

  if [ 0 -ne "$g_log_enable_timestamps" ]; then
    local timestamp
    if [ 0 -eq "$g_log_use_precise_timestamps" ]; then
      timestamp="$(date -Iseconds)"
    else
      timestamp="$(date -Ins)"
    fi
    tag="$timestamp/$tag"
  fi

  if [ 0 -eq "$g_log_enable_ansi" ]; then
    style_start=""
    style_end=""
  fi

  tag="${prefix}${style_start}[$tag]:${style_end}"

  if [ "$log_level" -lt 5 ]; then
    shift 2
    if [ 0 -ne "$g_log_sink_is_fd" ]; then
      echo -e "$tag $*" >&"$g_log_sink"
    else
      echo -e "$tag $*" >>"$g_log_sink"
    fi
  else
    local exit_code="$3"
    local eval_code="$4"

    shift 4

    if [ 0 -ne "$g_log_error_sink_is_fd" ]; then
      echo -e "$tag $*" >&"$g_log_error_sink"
    else
      echo -e "$tag $*" >>"$g_log_error_sink"
    fi

    eval "$eval_code"

    exit "$exit_code"
  fi
}

# Check if the provided string is a numeric string. A numeric string contains
# only digits from 0 to 9 and and optional single hyphen (-) at index 0. Also
# a numeric string cannot be empty.
#
# PARAMETERS
#   1. The string you want to check.
#
# EXIT CODE
#   0 if the string is numeric, else 1.
function is_numeric_string() {
  local str=$1

  # If the first character is a minus, then remove it
  if [ "-" = "${str:0:1}" ]; then
    str=${str:1}
  fi

  # Check that str only contains digits
  expr "$str" : '^[0-9]*$' >/dev/null
}

# Clamp a number to a range between a specific lower and upper limit.
#
# The lower limit must be less than, or equal to the upper limit, everything
# else is undefined behaviour.
#
# PARAMETERS
#   1. The number that should be clamped.
#   2. The lower limit.
#   3. The upper limit.
#
# RETURNS
#   The lower limit, if the number is less than the lower limit,
#   the upper limit, if the number is greater than the upper limit,
#   otherwise the number.
function clamp() {
  echo "
    define clamp(value, min, max) {
      if (value < min) return min;
      if (value > max) return max;
      return value;
    }

    clamp(($1), ($2), ($3))
  " | bc -l
}

# Get the currently configured value for nightlight.
#
# RETURNS
#   The current numeric value for nightlight.
#
# EXIT CODE
#   0 on success, else 1.
function get_nightlight() {
  # Disable the warning, because we want the word splitting of
  # $GSETTINGS_NIGHTLIGHT_KEY to happen.
  #
  # shellcheck disable=SC2086
  gsettings get $GSETTINGS_NIGHTLIGHT_KEY | cut -d" " -f2
}

# Change the currently configured value for nightlight.
#
# EXIT CODE
#   0 on success, else 1.
function set_nightlight() {
  # Disable the warning, because we want the word splitting of
  # $GSETTINGS_NIGHTLIGHT_KEY to happen.
  #
  # shellcheck disable=SC2086
  gsettings set $GSETTINGS_NIGHTLIGHT_KEY $1
}

# Convert the numeric nightlight value to percent.
#
# PARAMETERS
#   1. The numeric nightlight value between $MIN_NIGHTLIGHT and $MAX_NIGHTLIGHT.
#
# RETURNS
#   A percentage between 0 and 100, both inclusive.
function nightlight_to_percent() {
  # Example percent ($1 = 2600):
  #
  # percent -= MAX_NIGHTLIGHT
  #   (percent = 2600 - 1700 = 900)
  #
  # percent /= (MIN_NIGHTLIGHT - MAX_NIGHTLIGHT)
  #   (percent = 900 / (4700 - 1700) = 0.3)
  #
  # percent = 1 - percent
  #   (percent = 1 - 0.3 = 0.7)
  #
  # percent *= 100
  #   (percent = 0.7 * 100 = 70.0)

  local value
  value="$(clamp "$1" $MAX_NIGHTLIGHT $MIN_NIGHTLIGHT)"

  # The percentage with all fractions.
  local precise_percent
  precise_percent="$(echo "
      value=$value
      value-=$MAX_NIGHTLIGHT

      percent=value / ($MIN_NIGHTLIGHT - $MAX_NIGHTLIGHT)
      percent=1 - percent

      percent*=100

      percent
    " | bc -l)"

  echo "
    scale=1
    percent=$precise_percent
    percent/=1
    percent
  " | bc -l
}

# Convert the percent nightlight value to its' numeric value.
#
# PARAMETERS
#   1. The nightlight percentage between 0 and 100, both inclusive.
#
# RETURNS
#   A numeric value between $MAX_NIGHTLIGHT and $MIN_NIGHTLIGHT, both inclusive.
function nightlight_from_percent() {
  local percent
  percent="$(clamp "$1" 0 100)"

  echo "
    # Invert and convert input percent (70.0) to decimal (0.3).
    percent=100 - ($percent)
    percent/=100

    value=percent * ($MIN_NIGHTLIGHT - $MAX_NIGHTLIGHT)
    value+=$MAX_NIGHTLIGHT

    # Truncate value.
    scale=0
    value/=1

    value
  " | bc -l
}

######################## SCRIPT ########################

input_nightlight_percent=$1

# Print current nightlight and exit.
if [ -z "$input_nightlight_percent" ]; then
  value=$(get_nightlight)
  echo "Current nightlight is $(nightlight_to_percent "$value")% ($value)"
  exit $EXIT_SUCCESS
fi

if ! is_numeric_string "$input_nightlight_percent"; then
  log \
    5 \
    $EXIT_INVALID_ARG \
    "The argument must be a number between 0 and 100, both inclusive"
fi

clamped_percent="$(clamp "$input_nightlight_percent" 0 100)"
value="$(nightlight_from_percent "$clamped_percent")"

echo "Setting nightlight to $(nightlight_to_percent "$value")% ($value)"

set_nightlight "$value"
exit $?
