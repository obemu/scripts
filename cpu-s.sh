#! /bin/bash

# Check if this script is run as root.
if ([ 0 -ne $EUID ]); then
	echo "This script must be run as root."
	exit 1
fi

# i5-8250u has 8 Cores
# => {0..7}
for i in {0..7}; do
	sudo cpufreq-set -c $i -g powersave
	sudo cpufreq-set -c $i -u 1GHz
done

echo "Set all 8 Cores to governor powersave and max frequency of 1GHz!"

exit 0
