#! /bin/bash

# Example usage:
# sudo ./mount-dev.sh /dev/sda1 /device_mounting_point
#
# Parameters:
# 1. : This is the device you want to mount.
# 2. : This is the location where you want to mount the device.
#
# Author:
# Emanuel Oberholzer
#
# Copyright 2021:
# This script is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This script is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# See <https://www.gnu.org/licenses/>.

# Check if this script is run as root.
if [ 0 -ne $EUID ]; then
  echo "This script must be run as root."
  exit 1
fi

device=$1
mountingPoint=$2

if ! [ -e "$device" ]; then
  echo "The device: \"$device\" does not exist!"
  exit 2
fi

# If $mountingPoint does not exist, create it.
if ! [ -d $mountingPoint ]; then
  mkdir $mountingPoint
fi

# Change owner of $mountingPoint.
chown $USER $mountingPoint

# Change group of $mountingPoint.
chgrp $USER $mountingPoint

# Remove all read/write/execute permissions
# from all users.
chmod a=- $mountingPoint

# Add read/write/execute permission for the
# user who owns the $mountingPoint.
chmod u=rwx $mountingPoint

mount $device $mountingPoint

exit 0
