#! /bin/bash

workspaceRoot=$1

array=($(echo $workspaceRoot | tr "/" "\n"))
parentLength=${#array[@]}

array=($(echo $2 | tr "/" "\n"))

projectDir=$workspaceRoot/${array[$parentLength]}

cd $projectDir

# Create output directories for cmake
if ([ -d bin ]); then
  rm -rf bin
fi
mkdir bin

if ([ -d build ]); then
  rm -rf build
fi
mkdir build

if ([ -d lib ]); then
  rm -rf lib
fi
mkdir lib

# Nagivate into build directory and execute cmake.
cd $projectDir/build
cmake .. -DCMAKE_BUILD_TYPE=Debug -DCMAKE_INSTALL_PREFIX=..

# Execute make in build directory.
make && make install

exit 0
