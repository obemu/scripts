#! /bin/bash

# Parameters
#
# first: (string, default = $(pwd) )
# Path to the project directory without trailing slash.
#
# second: (string, default = "main" )
# Name of the executable.

# Function definitions
function createDirectory() {
  echo "Creating $1 directory."
  mkdir "$1"
}

function logExecutingIn() {
  echo "Executing \"$1\" in \"$2\"."
  echo
}

projectDir=$1
executableName=$2

# Apply default values for empty parameters.
if [ -z "$projectDir" ]; then
  # pwd has no trailing slash.
  projectDir=$(pwd)
  echo "No argument for the projectDir was given, defaulting to $projectDir."
fi

# projectDir may have trailing slash, remove it.
# shellcheck disable=SC2001
# shellcheck disable=SC2086
projectDir="$(echo $projectDir | sed 's:/*$::')"

if [ -z "$executableName" ]; then
  executableName="main"
  echo "No argument for the executableName was given, defaulting to $executableName."
fi

buildDir=$projectDir/build
binaryDir=$projectDir/bin

# Check if necessary cmake directories exist,
# if they do delete them before recreating them.
# Also execute cmake in buildDir
if ! [ -d "$binaryDir" ]; then
  createDirectory "$binaryDir"
fi

if ! [ -d "$projectDir/lib" ]; then
  createDirectory "$projectDir/lib"
fi

if ! [ -d "$buildDir" ]; then
  createDirectory "$buildDir"
  cd "$buildDir" || exit 1
  logExecutingIn "cmake .." "$buildDir"
  cmake ..
fi

# Move into buildDir and execute make
cd "$buildDir" || exit 1

logExecutingIn "make" "$buildDir"
make && make install
echo

# Set the path to the executable
pathToExecutable=$binaryDir/$executableName

# Check if pathToExecutable is executable
if ! [ -x "$pathToExecutable" ]; then
  echo "main is not executable!"
  echo
  exit 2
fi

logExecutingIn $executableName "$binaryDir"
$pathToExecutable
echo

exit 0
