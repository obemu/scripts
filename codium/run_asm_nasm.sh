#! /bin/bash

# Constants.
readonly ASSEMBLER="nasm"
readonly LINKER="ld"
readonly OUTPUT_FILE_EXTENSION="obj"
readonly OUTPUT_DIRECTORY_NAME="build"

# Function definitions.
function error() {
  echo "ERROR!"
  echo $2
  echo

  exit $1
}

function execute() {
  tool=$1
  args=$2
  exitCode=$3
  errorMsg=$4

  $tool $args

  if ([ "0" != "$?" ]); then
    error $exitCode "$errorMsg"
  fi
}

# Get input.
fullFileNameWithExt=$1 # e.g.: /home/emanuel/Projects/asmProject/example.asm
x86=$2                 # 1 for 32 bit Mode, 0 for 64 Bit Mode

# Split the fullFileNameWithExt into its name and its extension.
IFS=. read -ra fileNameParts <<<$fullFileNameWithExt
fullFileNameWithoutExt=${fileNameParts[0]}
fileExtension=${fileNameParts[1]}

# Get the path to the file directory.
IFS=/ read -ra filePathParts <<<$fullFileNameWithoutExt
filePathPartsLength=${#filePathParts[@]}
workingDirectory=""
for ((i = 1; i < $filePathPartsLength - 1; i++)); do
  workingDirectory="$workingDirectory/${filePathParts[$i]}"
done

# Setup variables.
fileNameWithoutExt=${filePathParts[$filePathPartsLength - 1]}
outputDirectory="$workingDirectory/$OUTPUT_DIRECTORY_NAME"
fullOutputFileNameWithoutExt="$outputDirectory/$fileNameWithoutExt"
fullOutputFileNameWithExt="$fullOutputFileNameWithoutExt.$OUTPUT_FILE_EXTENSION"

# Default to 32 Bit Mode.
if ([ -z $x86 ]); then
  x86="1"
fi

# Check if paths/directories exist.
if ! [ -d $workingDirectory ]; then
  error 1 "The directory $workingDirectory does not exist!"
fi

if ! [ -e $fullFileNameWithExt ]; then
  error 2 "The file $fullFileNameWithExt does not exist!"
fi

# Create ouputDirectory if it does not exist yet.
if ! [ -d $outputDirectory ]; then
  mkdir $outputDirectory
fi

# Fill args.
assemblerArgs="-g"
linkerArgs=""

if ([ "0" = $x86 ]); then
  assemblerArgs="$assemblerArgs -f elf64"
  linkerArgs="$linkerArgs -m elf_x86_64"
else
  assemblerArgs="$assemblerArgs -f elf32"
  linkerArgs="$linkerArgs -m elf_i386"
fi

assemblerArgs="$assemblerArgs $fullFileNameWithExt"
assemblerArgs="$assemblerArgs -o $fullOutputFileNameWithExt"

linkerArgs="$linkerArgs $fullOutputFileNameWithExt"
linkerArgs="$linkerArgs -o $fullOutputFileNameWithoutExt"

# Execute tools.
execute "cd" "$workingDirectory" "Caused by: cd $workingDirectory" 3
execute "$ASSEMBLER" "$assemblerArgs" "Caused by: $ASSEMBLER $assemblerArgs" 4
execute "$LINKER" "$linkerArgs" "Caused by: $LINKER $linkerArgs" 5
execute "$fullOutputFileNameWithoutExt" "" "Caused by: $fullFileNameWithoutExt" 6

exit 0
