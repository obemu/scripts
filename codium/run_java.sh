#! /bin/bash

# shellcheck disable=SC2086



# Parameters
#
# first: workspaceRoot (string)
# eg.: /home/USER/Projects/MyJavaProject
#
# second: mainWithPackage (string)
# eg.: com.example.test.Main
#
# third: librariesString (string)
# eg.: lib1.jar,lib2.jar,lib3.jar
# Providing "0" as an argument for librariesString is
# the same as providing no libraries.
#
# fourth: extraOptions (string)
# Extra options which the user may want to pass to the "java" command



######################## Function definitions ########################
function error() {
  echo "ERROR!"
  echo $2
  echo

  exit $1
}


######################## Script ########################

workspaceRoot=$1   
mainWithPackage=$2
librariesString=$3
extraOptions=$4

hasLibraries="1"
if [ "0" = "$librariesString" ]; then
  hasLibraries=0
fi

# Check if workspaceRoot exists, if not throw an error.
if ! [ -d "$workspaceRoot" ]; then
  error 1 "The directory \"$workspaceRoot\" does not exist!"
fi

sourceDir="$workspaceRoot/src"
libraryDir="$workspaceRoot/lib"
binaryDir="$workspaceRoot/bin"

# Check if sourceDir exists, if not throw an error.
if ! [ -d "$sourceDir" ]; then
  error 2 "The directory \"$sourceDir\" does not exist!"
fi

# If hasLibraries is true and  libraryDir does
# not exist, throw an error.
if [ "0" != $hasLibraries ] && [ ! -d "$libraryDir" ]; then
  error 3 "Libraries are provided, but the library directory \"$libraryDir\" does not exist!"
fi

# Check if binaryDir exists, if not create it.
if ! [ -d "$binaryDir" ]; then
  echo "Creating directory \"$binaryDir\"."
  mkdir "$binaryDir"
fi

if [ "0" != $hasLibraries ]; then

  # Split librariesString by the commas seperating each
  # and library filename and store them in the
  # libraries array.
  IFS=, read -ra libraries <<<$librariesString
  classPaths=""

  # Check if each library file exists, if not throw an error.
  for ((i = 0; i < ${#libraries[@]}; i++)); do
    libFile="$libraryDir/${libraries[$i]}"

    if ! [ -e $libFile ]; then
      error 4 "The library file \"$libFile\" does not exist!"
    fi

    if [ -z $classPaths ]; then
      classPaths="$libFile"
    else
      classPaths="$classPaths:$libFile"
    fi

  done

fi

# Get path to main java file
# (The file that contains the main function).
relativePathToMainFileWithoutExtension=${mainWithPackage//./"/"}
fullPathToMainFile="$sourceDir/$relativePathToMainFileWithoutExtension.java"

# Check if main java file exists, if not throw an error.
if ! [ -e "$fullPathToMainFile" ]; then
  error 5 "The file containing the main function \"$fullPathToMainFile\" does not exist!"
fi

# Compile everything to .class files.
# Wrap classPaths in quotes in case it is empty.
javac -d "$binaryDir" -sourcepath "$sourceDir" -cp "$classPaths" "$fullPathToMainFile"

# Execute the main file, that contains the main function.
java "$extraOptions" -cp "$binaryDir:$classPaths" "$mainWithPackage"

exit 0
