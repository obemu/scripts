#! /bin/bash

######################## Constants ########################
readonly VERSION=1
readonly TEST_FILE_ENDING="_test"

# See https://en.wikipedia.org/wiki/ANSI_escape_code#Colors
readonly TEXT_STYLE_END="\033[m"
readonly TEXT_STYLE_ERROR="\033[1;31m"
readonly TEXT_STYLE_INFO="\033[0;36m"

######################## Function definitions ########################
function error() {
  echo -e $TEXT_STYLE_ERROR"ERROR! $TEXT_STYLE_END"
  echo $2
  echo

  exit $1
}

######################## Variables ########################
workspaceFolder=$1
fullFilePath=$2
# workspaceFolder="/media/emanuel/EVO_860/Projects/App_Spuernasenecke/flutter_app/experimente"
# fullFilePath="/media/emanuel/EVO_860/Projects/App_Spuernasenecke/flutter_app/experimente/test/unit/bluetooth_test.dart"

# Check if arguments are not empty.
if ([ -z $workspaceFolder ]); then
  error 1 "No argument for the workspaceFolder was given."
fi

if ([ -z $fullFilePath ]); then
  error 2 "No argument for the fullFilePath was given."
fi

# Check if files/dirs exists.
if !([ -d $workspaceFolder ]); then
  error 3 "The directory \"$workspaceFolder\" does not exist!"
fi

if ([ -x $fullFilePath ]); then
  error 4 "The file \"$fullFilePath\" does not exist!"
fi

# Get the filepath relative to $workspaceFolder.
relativeFilePath=${fullFilePath/$workspaceFolder/""}

# Check if $fullFilePath is within $workspaceFolder.
if ([ "$workspaceFolder$relativeFilePath" != $fullFilePath ]); then
  error 5 "The file \"$fullFilePath\" has to be in the directory \"$workspaceFolder\"!"
fi

# Get the file name with and without extension and get
# the file extension.
fileNameWithExt=${fullFilePath##*/}
fileNameWithoutExt=${fileNameWithExt%.*}
fileExtension=${fileNameWithExt##*.}

# Check if $fileNameWithoutExt ends with $TEST_FILE_ENDING.
#
# $fileNameEnd equals $fileNameWithoutExt, if $fileNameWithoutExt
# does not end with $TEST_FILE_ENDING.
fileNameEnd=${fileNameWithoutExt%$TEST_FILE_ENDING}
if ([ $fileNameWithoutExt = $fileNameEnd ]); then
  error 6 "The filename must end with \"$TEST_FILE_ENDING\", filename: \"$fileNameWithExt\"."
fi

echo -e "Testing \"$TEXT_STYLE_INFO$relativeFilePath$TEXT_STYLE_END\" in workspace folder \"$TEXT_STYLE_INFO$workspaceFolder$TEXT_STYLE_END\". \n"

cd $workspaceFolder
flutter test $fullFilePath

exit 0
