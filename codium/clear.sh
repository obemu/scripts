#! /bin/bash

function removeDirectory() {
  rm -rf "$1"
}

function removeFile() {
  rm -f "$1"
}

target=$1

if ! [ -d "$target" ]; then
  echo "Error"
  echo "The directory $target does not exist!"
  exit 1
fi

echo "Clearing $target of any build files..."

# Remove all "build" directories.
buildFiles=$(find $target -type d -iname "build")
for file in $buildFiles; do
  removeDirectory "$file"
done

# Remove all "*.out" files.
buildFiles=$(find $target -type f -iname "*.out")
for file in $buildFiles; do
  removeFile "$file"
done

# Remove all "*.obj" files.
buildFiles=$(find $target -type f -iname "*.obj")
for file in $buildFiles; do
  removeFile "$file"
done

# Remove all "*.exe" files.
buildFiles=$(find $target -type f -iname "*.exe")
for file in $buildFiles; do
  removeFile "$file"
done

echo "Done!"

exit 0
