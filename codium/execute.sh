#! /bin/bash

# Check the spelling of a file with aspell and print
# the lines of the unknown/error causing words.
# Copyright (C) 2021 Emanuel Oberholzer
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

# For help use
# execute.sh -h or execute.sh --help

######################## Constants ########################

readonly REPORT_EMAIL="emanueloberholzer400@gmail.com"

readonly PROGRAM_NAME="execute.sh"
readonly VERSION="1.0.0"

readonly TEXT_STYLE_END="\033[m"
readonly TEXT_STYLE_BOLD="\033[1;37m"

######################## Function definitions ########################

# Parameters
#
# first: workspaceRoot (string)
#
# second: projectDir (string)
# If isMultipleProjectsWorkspace is 0 (false), then this
# parameter is not taken into consideration.
#
# third: isRunScriptInProjectDir (int)
#
# fourth: isMultipleProjectsWorkspace (int)
#
# fifth: executableName (string)
# The name of the compiled binary, if this parameter is
# not given, then it defaults to "main"
function help() {
  function bold() {
    echo -e "${TEXT_STYLE_BOLD}$1${TEXT_STYLE_END}"
  }
  function tab() {
    echo -e "\t$1"
  }

  bold "VERSION"
  tab "$PROGRAM_NAME v$VERSION"
  echo

  bold "SYNOPSIS"
  tab "$PROGRAM_NAME workspaceRoot projectDir isRunScriptInProjectDir isMultipleProjectsWorkspace executableName"
  echo

  bold "DESCRIPTION"
  echo

  bold "OPTIONS"
  echo

  bold "EXAMPLE"
  echo

  bold "SEE ALSO"
  tab "Report bugs to: $REPORT_EMAIL"

  exit 0
}

######################## Script ########################

if [ 0 == $# ] || [ "-h" = "$1" ] || [ "--help" = "$1" ]; then
  help
fi

workspaceRoot=$1
isRunScriptInProjectDir=$3
isMultipleProjectsWorkspace=$4
executableName=$5

# Apply default values for empty parameters
if [ -z "$isRunScriptInProjectDir" ]; then
  isRunScriptInProjectDir="0"
fi

if [ -z "$isMultipleProjectsWorkspace" ]; then
  isMultipleProjectsWorkspace="0"
fi

if [ "0" = "$isMultipleProjectsWorkspace" ]; then
  projectDir=$workspaceRoot
else
  array=("")

  # Remove empty strings from array.
  function removeEmptyStrings() {
    for ((i = 0; i < "${#array[@]}"; i++)); do
      if [ -z "${array[$i]}" ]; then
        unset "array[$i]"
      fi
    done
  }

  IFS="/" read -r -a array <<<"$workspaceRoot"
  removeEmptyStrings
  parentLength=${#array[@]}

  IFS="/" read -r -a array <<<"$2"
  removeEmptyStrings

  projectDir="$workspaceRoot/${array[$parentLength + 1]}"
fi

# Line break
echo

# Set the path to run.sh
if [ "0" = "$isRunScriptInProjectDir" ]; then
  pathToRunSH="/home/emanuel/Projects/Scripts/codium/run.sh"
else
  pathToRunSH="$projectDir/run.sh"
fi

# Check if run.sh exists.
if ! [ -x "$pathToRunSH" ]; then
  echo "The file $pathToRunSH does not exist!"
  echo
  exit 1
fi

# Execute run.sh
$pathToRunSH "$projectDir $executableName"

exit 0
