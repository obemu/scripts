#! /bin/bash

codiumExtensionsDir="$HOME/.vscode-oss/extensions"
codeExtensionsDir="$HOME/.vscode/extensions"

if ! [ -d "$codeExtensionsDir" ]; then
  echo "ERROR! The directory $codeExtensionsDir does not exist."
  exit 1
fi

# Loop through every file or directory in codeExtensionsDir directory

# shellcheck disable=SC2207
codeExtensions=($(ls "$codeExtensionsDir"))
# shellcheck disable=SC2207
codiumExtensions=($(ls "$codiumExtensionsDir"))

codeExtensionsLength=${#codeExtensions[@]}
codiumExtensionsLength=${#codiumExtensions[@]}

for ((i = 0; i < codeExtensionsLength; i++)); do
  codeExt=${codeExtensions[$i]}

  # Check if codiumExtensionsDir already contains codeExt
  contains="false"
  for ((j = 0; j < codiumExtensionsLength; j++)); do
    codiumExt=${codiumExtensions[$i]}
    if [ "$codiumExt" = "$codeExt" ]; then
      contains="true"
      break
    fi
  done

  echo -n "[$((i + 1))/$codeExtensionsLength]: "
  if [ "true" = "$contains" ]; then
    echo "Codium already contains \"$codeExt\"."
    continue
  fi

  src="$codeExtensionsDir/$codeExt"
  echo "Copying \"$codeExt\" to \"$codiumExtensionsDir\""
  cp -r "$src" "$codiumExtensionsDir"
done

echo -e "\nFinished updating \"$codiumExtensionsDir\" from source \"$codeExtensionsDir\"."

exit 0
