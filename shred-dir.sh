#! /bin/bash

readonly DIR=$1

if ! [ -d "$DIR" ]; then
  echo "The directory '$DIR' does not exist!"
  exit 1
fi

find "$DIR" -type f -exec shred -z -u "{}" \;
rm -rf "$DIR"
