#! /bin/bash

# Check the spelling of a file with aspell and print
# the lines of the unknown/error causing words.
# Copyright (C) 2021 Emanuel Oberholzer
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

# For help use
# spell-check -h or spell-check --help

# shellcheck disable=SC2002

######################## Constants ########################

readonly REPORT_EMAIL="emanueloberholzer400@gmail.com"

readonly VERSION="1.0.0"

readonly DEFAULT_FILTER_MODE="tex"

# See https://en.wikipedia.org/wiki/ANSI_escape_code#Colors
readonly TEXT_STYLE_END="\033[m"
readonly TEXT_STYLE_ERROR="\033[1;31m"
readonly TEXT_STYLE_INFO="\033[1;4;33m"

######################## Function definitions ########################

function error() {
  echo -e "${TEXT_STYLE_ERROR}ERROR!${TEXT_STYLE_END}"
  echo -e "$2"
  echo

  exit "$1"
}

function version() {
  echo "spell-check v$VERSION"
}

function help() {
  version

  echo "Usage: spell-check FILE [FILTER_MODE]"
  echo "Check the spelling of a file with aspell and print the lines of the unknown/error causing words."
  echo "Example: spell-check info.txt none"
  echo "If the input file is written in LaTeX, then the mode can be omitted, because the default is \"$DEFAULT_FILTER_MODE\"."
  echo

  echo "Possible filter modes: \"none\", \"url\", \"email\", \"html\", \"tex\" or \"nroff\""
  echo

  echo "Report bugs to: $REPORT_EMAIL"

  exit 0
}

######################## Script ########################

file=$1
filterMode=$2

if [ -z "$1" ] || [ "-h" = "$1" ] || [ "--help" = "$1" ]; then
  help
fi

if ! [ -e "$file" ]; then
  error 1 "The provided input file \"$file\" does not exist."
fi

# Mode is one of "none", "url", "email", "html", "tex" or "nroff".
if [ -z "$filterMode" ]; then
  filterMode="--mode=$DEFAULT_FILTER_MODE"
else
  filterMode="--mode=$filterMode"
fi

# Dont specify the language, default to the
# users local configuration for aspell.
errorWords=$(cat "$file" | aspell $filterMode list)

for item in $errorWords; do
  echo -e "$TEXT_STYLE_INFO$item$TEXT_STYLE_END"
  cat "$file" | grep -s -n --color=always "$item"
  echo
  echo
done

exit 0
