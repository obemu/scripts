#! /bin/bash

# see https://superuser.com/a/1391115

readonly DIR=$1

if ! [ -d "$DIR" ]; then
  echo "The directory '$DIR' does not exist!"
  exit 1
fi

find "$DIR" -type f -print0 | xargs -0 -P0 -n1 sha256sum | sort -k 2 | sha256sum | cut -d " " -f 1
