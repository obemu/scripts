#! /bin/bash

buildDir=$1

if [ -d "$buildDir" ]; then
  # navigate to buildDir
  cd "$buildDir"

  # execute cmake in buildDir
  cmake ..
else
  echo "The directory \"$buildDir\" does not exist on your machine!"
fi

exit