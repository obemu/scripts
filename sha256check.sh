#! /bin/bash

###### CONSTANTS ######

readonly PROGNAME="sha256check"
readonly VERSION="1.0.0"

# https://en.wikipedia.org/wiki/ANSI_escape_code#Colors
readonly ANSI_END="\x1B[0m"
readonly ANSI_FG_RED="\x1B[38;5;196m"

readonly EXIT_OK=0
readonly EXIT_NOT_OK=1
readonly EXIT_SUM_INVALID=2
readonly EXIT_FILE_NOT_EXIST=3

###### SCRIPT ######

sum=$1
file=$2

# Print help.
if [ -z "$sum" ] && [ -z "$file" ]; then
  echo "${PROGNAME}"
  echo
  echo -e "${ANSI_FG_RED}NAME${ANSI_END}"
  echo -e "\t${PROGNAME} version ${VERSION}"
  echo
  echo -e "${ANSI_FG_RED}DESCRIPTION${ANSI_END}"
  echo -en "\tThis script compares a given SHA256SUM against "
  echo "the calculated sum of a file."
  echo
  echo -e "${ANSI_FG_RED}ARGUMENTS${ANSI_END}"
  echo -en "\tThe first argument has to be a SHA256SUM and the "
  echo -en "second argument has to be the path to the file you "
  echo "want to compare the sum against."
  echo
  echo -e "${ANSI_FG_RED}EXAMPLE${ANSI_END}"
  echo -e "\t${ANSI_FG_RED}${PROGNAME} a4fc2740bb0272892315ce2a4e762a51b1c3f37b9224742749f13be8de10331a myfile.txt${ANSI_END}"
  echo
  echo -en "\tCompare the sum 'a4fc2740bb0272892315ce2a4e762a51b1c3f37b9224742749f13be8de10331a' "
  echo -e "against the file 'file.txt'."
  echo
  echo -e "${ANSI_FG_RED}EXIT CODES${ANSI_END}"
  echo -e "\t${EXIT_OK}\tThe provided sum matches the sum of the file.\n"
  echo -e "\t${EXIT_NOT_OK}\tThe provided sum does not match the sum of the file.\n"
  echo -e "\t${EXIT_SUM_INVALID}\tThe provided sum is invalid.\n"
  echo -e "\t${EXIT_FILE_NOT_EXIST}\tThe file does not exist.\n"

  exit $EXIT_OK
fi

# Check if $sum is valid.
if ! [ 64 -eq "$(echo -n "$sum" | wc -c)" ]; then
  echo
  echo "No valid SHA256SUM that can be compared against a file was provided."
  if [ -n "$sum" ]; then
    echo "sum: '$sum'"
  fi
  exit $EXIT_SUM_INVALID
fi

# Check if $file exists.
if ! [ -e "$file" ]; then
  echo
  echo "The file '$file' does not exist."
  exit $EXIT_FILE_NOT_EXIST
fi

# Compare $sum against $file.
echo -n "$sum  $file" | sha256sum -c >/dev/null 2>&1
shasumExitcode=$?

if [ 0 -ne "$shasumExitcode" ]; then
  echo
  echo "The SHA256SUMs do not match! ❌"
  exit $EXIT_NOT_OK
fi

echo
echo "The SHA256SUMs match! ✅"
exit $EXIT_OK
