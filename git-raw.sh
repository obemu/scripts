#! /bin/bash

# shellcheck disable=2181

###### Constants ######

readonly PROGNAME="git-raw"
readonly VERSION="1.0.2"

readonly EXIT_OK=0
readonly EXIT_UNSUPPORTED_HOST=1
readonly EXIT_OUTFILE_ALREADY_EXISTS=2
readonly EXIT_GET_FAILED=3

# https://en.wikipedia.org/wiki/ANSI_escape_code#Colors
readonly ANSI_END="\x1B[0m"
readonly ANSI_FG_RED="\x1B[38;5;196m"

###### Functions ######

function help() {
  echo "${PROGNAME}"
  echo
  echo -e "${ANSI_FG_RED}NAME${ANSI_END}"
  echo -e "\t${PROGNAME} version ${VERSION}"
  echo
  echo -e "${ANSI_FG_RED}SYNOPSIS${ANSI_END}"
  echo -en "\t${ANSI_FG_RED}${PROGNAME}${ANSI_END} "
  echo -en "${ANSI_FG_RED}<url>${ANSI_END} " # 1. argument
  echo -e "[ ${ANSI_FG_RED}<outfile>${ANSI_END} ]" # 2. argument
  echo
  echo -e "${ANSI_FG_RED}DESCRIPTION${ANSI_END}"
  echo -en "\tThis script downloads the file at ${ANSI_FG_RED}url${ANSI_END} from GitLab/GitHub into "
  echo -e "the current working directory."
  echo
  echo -e "${ANSI_FG_RED}ARGUMENTS${ANSI_END}"
  echo -en "\t${ANSI_FG_RED}url${ANSI_END}"
  echo -e "\tThe url of the file in a GitLab/GitHub respository."
  echo
  echo -en "\t${ANSI_FG_RED}outfile${ANSI_END}"
  echo -e "\tThe path to the output file."
  echo -e "\t\tDefaults to the name of the file at url."
  echo -e "\t\tIf this argument is set to ${ANSI_FG_RED}-${ANSI_END}, then the output is written to stdout."
  echo
  echo -e "${ANSI_FG_RED}EXAMPLE 1${ANSI_END}"
  echo -e "\t${ANSI_FG_RED}${PROGNAME} 'https://gitlab.com/asdoi/quick-tiles/-/blob/master/.gitignore'${ANSI_END}"
  echo
  echo -e "\tDownload the file '.gitignore' from the respository 'quick-tiles' into the "
  echo -e "\tcurrent working directory. If '.gitignore' already exists then the downloaded "
  echo -e "\tfile gets renamed to '.gitignore.1'."
  echo
  echo -e "${ANSI_FG_RED}EXAMPLE 2${ANSI_END}"
  echo -e "\t${ANSI_FG_RED}${PROGNAME} 'https://gitlab.com/asdoi/quick-tiles/-/blob/master/.gitignore' myGitignore${ANSI_END}"
  echo
  echo -e "\tDownload the file '.gitignore' from the respository 'quick-tiles' into the "
  echo -e "\tcurrent working directory and rename it to 'myGitignore'. If a file called "
  echo -e "\t'myGitignore' already exists then the user gets asked if it should be overwritten."
  echo
  echo -e "${ANSI_FG_RED}EXAMPLE 3${ANSI_END}"
  echo -e "\t${ANSI_FG_RED}${PROGNAME} 'https://gitlab.com/asdoi/quick-tiles/-/blob/master/.gitignore' -${ANSI_END}"
  echo
  echo -e "\tDownload the file '.gitignore' from the respository 'quick-tiles' and write "
  echo -e "\tthe output to stdout."
  echo
  echo -e "${ANSI_FG_RED}EXIT CODES${ANSI_END}"
  echo -e "\t${EXIT_OK}\tThe file has successfully been downloaded.\n"
  echo -e "\t${EXIT_UNSUPPORTED_HOST}\tThe host of the respository is not supported.\n"
  echo -e "\t${EXIT_OUTFILE_ALREADY_EXISTS}\tThe output file already exists and it cannot be overwritten.\n"
  echo -e "\t${EXIT_GET_FAILED}\tDownloading the file failed.\n"

  exit $EXIT_OK
}

# Get the protocol from an url.
#
# Example:
# "https://website.com/files/info.txt" -> "https://"
function getProtocol() {
  # See https://gist.github.com/joshisa/297b0bc1ec0dcdda0d1625029711fa24
  echo "$1" | grep :// | sed -e's,^\(.*://\).*,\1,g'
}

# Get the host from an url.
#
# Example:
# "https://website.com/files/info.txt" -> "website.com"
function getHost() {
  declare url
  url="$(removeProtocol "$1")"

  echo "${url%%/*}"
}

# Remove the protocol from an url.
#
# Example:
# "https://website.com/files/info.txt" -> "website.com/files/info.txt"
function removeProtocol() {
  declare -r proto="$(getProtocol "$1")"
  declare -r url="${1/$proto/}"

  echo "$url"
}

# Convert the link of a GitHub file to one that gives you access to the files raw content.
#
# Example 1:
# "https://github.com/momshaddinury/input_form_field/blob/7a0669f572e66cb26c4db9a364dceac018db745f/LICENSE"
# becomes
# "https://raw.githubusercontent.com/momshaddinury/input_form_field/7a0669f572e66cb26c4db9a364dceac018db745f/LICENSE"
#
# Example 2:
# "https://github.com/momshaddinury/input_form_field/blob/main/LICENSE"
# becomes
# "https://raw.githubusercontent.com/momshaddinury/input_form_field/main/LICENSE"
function getGitHub() {
  declare url
  url="$(removeProtocol "$1")"

  declare -a parts
  IFS="/" read -r -a parts <<<"$url"

  declare rawUrl="https://raw.githubusercontent.com"

  # user name
  rawUrl="$rawUrl/${parts[1]}"

  # project name
  rawUrl="$rawUrl/${parts[2]}"

  declare -r partsLength=${#parts[@]}
  for ((i = 4; i < "$partsLength"; i++)); do
    rawUrl="$rawUrl/${parts[$i]}"
  done

  echo "$rawUrl"
}

# Convert the link of a GitLab file to one that gives you access to the files raw content.
#
# Example 1:
# "https://gitlab.com/asdoi/quick-tiles/-/blob/master/.gitignore"
# becomes
# "https://gitlab.com/asdoi/quick-tiles/-/raw/master/.gitignore"
#
# Example 2:
# "https://gitlab.com/asdoi/quick-tiles/-/blob/b8869ee29acfc71d9a1fba9fd7f39c1b3e8bc8c9/.gitignore"
# becomes
# "https://gitlab.com/asdoi/quick-tiles/-/raw/b8869ee29acf/.gitignore"
function getGitLab() {  
  declare url
  url="$(removeProtocol "$1")"

  declare -a parts
  IFS="/" read -r -a parts <<<"$url"

  declare rawUrl="https://gitlab.com"

  # user name
  rawUrl="$rawUrl/${parts[1]}"

  # project name
  rawUrl="$rawUrl/${parts[2]}"

  rawUrl="$rawUrl/${parts[3]}/raw"

  declare -r partsLength=${#parts[@]}
  for ((i = 5; i < "$partsLength"; i++)); do
    rawUrl="$rawUrl/${parts[$i]}"
  done

  echo "$rawUrl"
}

###### Script ######

url="$1"

if [ -z "$url" ] || [ "-h" = "$url" ] || [ "--help" = "$url" ]; then
  help
fi

# Convert $url to an url that points to the content of the file at $url,
# instead of at an html page.
case "$(getHost "$url")" in
"gitlab.com") rawUrl="$(getGitLab "$url")" ;;
"github.com") rawUrl="$(getGitHub "$url")" ;;
*) exit $EXIT_UNSUPPORTED_HOST ;;
esac

outfile="$2"
if [ -e "$outfile" ]; then
  echo -n "The file '$outfile' already exists, overwrite? [Y/N]: "
  read -r shouldOverwrite

  if [ "y" != "$shouldOverwrite" ] && [ "Y" != "$shouldOverwrite" ]; then
    echo "Aborting, because '$outfile' cannot be overwritten."
    exit $EXIT_OUTFILE_ALREADY_EXISTS
  fi
fi

# Download file at $rawUrl
if [ -z "$outfile" ]; then
  wget -q --show-progress "$rawUrl"
else
  wget -q --show-progress -O "$outfile" "$rawUrl"
fi

# Check if download was successful.
if [ 0 -ne $? ]; then
  exit $EXIT_GET_FAILED
fi

exit $EXIT_OK
