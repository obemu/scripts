#! /bin/bash

# Script for simple usage of the brscan-skey tool
# Copyright (C) 2022 Emanuel Oberholzer

# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.

# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.

# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

######################## Constants ########################

readonly REPORT_EMAIL="emanueloberholzer400@gmail.com"
readonly AUTHOR="Emanuel Oberholzer"

readonly PROGRAM_NAME="latex_font_arg"
readonly VERSION="1.0.0"

# https://en.wikipedia.org/wiki/ANSI_escape_code#Colors
readonly TEXT_STYLE_END="\x1B[0m"
readonly TEXT_STYLE_BOLD="\x1B[1;37m"
readonly TEXT_STYLE_FG_ERROR="\x1B[38;5;196m"
readonly TEXT_STYLE_FG_WARNING="\x1B[4;38;5;208m"
readonly TEXT_STYLE_FG_INFO="\x1B[38;5;46m"
readonly TEXT_STYLE_FG_VERBOSE=$TEXT_STYLE_BOLD

# Used in function log()
logLevel=1

######################## Functions ########################

# Print a log message to stdout.
#
# PARAMETERS
#   1. The log level of the message(=$1)
#
# If the message's log level is error
#   2. Exit code for the script(=$2)
#   3. The message that should be printed(=$3)
#
# If the message's log level is NOT error
#   2. The message that should be printed(=$2)
#
# LOG LEVELS
# verbose -> 1
# debug -> 2
# info -> 3
# warning -> 4
# error -> 5
#
# EXAMPLE
# log 3 "My info message"
# log 5 24 "My error message with exit code 24"
function log() {
  if [ "$logLevel" -gt "$1" ]; then
    return
  fi

  # verbose
  if [ "$1" -eq 1 ]; then
    echo -e "${TEXT_STYLE_FG_VERBOSE}VERBOSE:${TEXT_STYLE_END} $2"
    return
  fi

  # debug
  if [ "$1" -eq 2 ]; then
    return
  fi

  # info
  if [ "$1" -eq 3 ]; then
    echo -e "${TEXT_STYLE_FG_INFO}INFO:${TEXT_STYLE_END} $2"
    return
  fi

  # warning
  if [ "$1" -eq 4 ]; then
    echo -e "${TEXT_STYLE_FG_WARNING}WARNING:${TEXT_STYLE_END} $2"
    return
  fi

  # error
  if [ "$1" -eq 5 ]; then
    echo -e "${TEXT_STYLE_FG_ERROR}ERROR!${TEXT_STYLE_END}"
    echo -e "$3"
    echo

    exit "$2"
  fi
}

# Print info at the start of the script.
function info() {
  echo "$PROGRAM_NAME v$VERSION"
  echo "Author: $AUTHOR"
  echo "Report Email: $REPORT_EMAIL"
  echo
}

function help() {
  info

  echo "Provide a font series with -se SERIES and a font "
  echo "shape with -sh SHAPE and a font file with -f FONT_FILE ."
  echo
  echo "Example:"
  echo "$PROGRAM_NAME -se SERIES -sh SHAPE -f MyFontFile.ttf"

  exit 0
}

# Check if the fontSeries(=$1) is a valid series.
function checkSeries() {
  local series=$1

  # When forming the series string from the weight and width, drop the
  # m that stands for medium weight or medium width, unless both weight
  # and width are m, in which case use just one (‘m’).
  #
  # The single 'm' is handled by $validTypicalSeries .
  if [ "mm" = "$series" ]; then
    log 5 101 "When forming the series string from the weight and width, drop the
m that stands for medium weight or medium width, unless both weight
and width are m, in which case use just one (‘m’)."
  fi

  # A series combines a weight and a width. Typically, a font supports
  # only a few of the possible combinations. Some common combined
  # series values include:
  #
  # m	Medium (normal)
  # b	Bold
  # c	Condensed
  # bc	Bold condensed
  # bx	Bold extended
  local validTypicalSeries=("m" "b" "c" "bc" "bx")

  # Check if $validTypicalSeries contains $series .
  for ((i = 0; i < ${#validTypicalSeries[@]}; i++)); do
    value=${validTypicalSeries[$i]}

    if [ "$series" = "$value" ]; then
      return
    fi
  done

  # The possible values for weight, individually, are:
  #
  # ul	Ultra light
  # el	Extra light
  # l	Light
  # sl	Semi light
  # m	Medium (normal)
  # sb	Semi bold
  # b	Bold
  # eb	Extra bold
  # ub	Ultra bold
  local validWeights=("ul" "el" "l" "sl" "m" "sb" "b" "eb" "ub")

  # The possible values for width, individually, are (the meaning and
  # relationship of these terms varies with individual typefaces):
  #
  # uc	Ultra condensed
  # ec	Extra condensed
  # c	Condensed
  # sc	Semi condensed
  # m	Medium
  # sx	Semi expanded
  # x	Expanded
  # ex	Extra expanded
  # ux	Ultra expanded
  local validWidths=("uc" "ec" "c" "sc" "m" "sx" "x" "ex" "ux")

  for ((i = 0; i < ${#validWeights[@]}; i++)); do
    weight=${validWeights[$i]}

    for ((j = 0; j < ${#validWidths[@]}; j++)); do
      width=${validWidths[$j]}

      # This case is handled at the top of the function.
      if [ "mm" = "$weight$width" ]; then
        continue
      fi

      if [ "$series" = "$weight$width" ]; then
        return
      fi

    done
  done

  log 5 102 "'$series' is not a valid font series."
}

# Check if the fontShape(=$1) is a valid shape.
function checkShape() {
  local shape=$1

  # n	 Upright (normal)
  # it	Italic
  # sl	Slanted (oblique)
  # sc	Small caps
  # ui	Upright italics
  # ol	Outline
  local validShapes=("n" "it" "sl" "sc" "ui" "ol")

  # Check if $validTypicalSeries contains $series .
  for ((i = 0; i < ${#validShapes[@]}; i++)); do
    value=${validShapes[$i]}

    if [ "$shape" = "$value" ]; then
      return
    fi
  done

  log 5 103 "'$shape' is not a valid font shape."
}

# Check if the fontFile(=$1) is a valid font file.
function checkFont() {
  local fontFile=$1

  if ! [ -e "$fontFile" ]; then
    log 5 104 "The file '$fontFile' does not exist."
  fi
}

######################## Script ########################

# No arguments passed
if [ -z "$*" ]; then
  help
fi

# Check if $# is a multiple of 2
((mod = $# % 2))
if [ 0 != $mod ]; then
  log 5 1 "Cannot provide an uneven amount of arguments."
fi

readonly args=("$@")
readonly argsLength=${#args[@]}

series="m"
shape="n"
font=""

# Get every value from each switch.
for ((i = 0; i < argsLength; i = i + 2)); do
  switch=${args[$i]}
  value=${args[((i + 1))]}

  case $switch in
  -se) series=$value ;;
  -sh) shape=$value ;;
  -f) font=$value ;;
  *) log 5 2 "'$switch' is an invalid switch." ;;
  esac
done

# Check if $font was provided.
if [ -z "$font" ]; then
  log 5 3 "You must provide a font with '-f'."
fi

# Get absolute path of $font .
font=$(realpath "$font")

checkSeries "$series"
checkShape "$shape"
checkFont "$font"

echo "$font:$series:$shape"

exit 0
