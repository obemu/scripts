#! /bin/bash

# Script for simple usage of the brscan-skey tool
# Copyright (C) 2022 Emanuel Oberholzer

# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.

# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.

# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

# http://xpt.sourceforge.net/techdocs/language/latex/latex33-LaTeXAndTrueTypeFont/ar01s03.html

#! test 1
# ./create_latex_font_v2.sh $(pwd)/out $(./latex_font_arg.sh -f ./fonts/LiberationSerif-Regular.ttf) $(./latex_font_arg.sh -se b -f ./fonts/LiberationSerif-Bold.ttf) $(./latex_font_arg.sh -sh it -f ./fonts/LiberationSerif-Italic.ttf) $(./latex_font_arg.sh -se b -sh it -f ./fonts/LiberationSerif-BoldItalic.ttf)
#! test 2
# ./create_latex_font_v2.sh $(pwd)/out $(./latex_font_arg.sh -f ./fonts/LiberationSerif-Regular.ttf)

######################## Constants ########################

readonly REPORT_EMAIL="emanueloberholzer400@gmail.com"
readonly AUTHOR="Emanuel Oberholzer"

readonly PROGRAM_NAME="create_latex_font_v2"
readonly VERSION="1.0.0"

# https://en.wikipedia.org/wiki/ANSI_escape_code#Colors
readonly TEXT_STYLE_END="\x1B[0m"
readonly TEXT_STYLE_BOLD="\x1B[1;37m"
readonly TEXT_STYLE_FG_ERROR="\x1B[38;5;196m"
readonly TEXT_STYLE_FG_WARNING="\x1B[4;38;5;208m"
readonly TEXT_STYLE_FG_INFO="\x1B[38;5;46m"
readonly TEXT_STYLE_FG_VERBOSE=$TEXT_STYLE_BOLD

# Used in function log()
logLevel=1

######################## Functions ########################

# Print a log message to stdout.
#
# PARAMETERS
#   1. The log level of the message(=$1)
#
# If the message's log level is error
#   2. Exit code for the script(=$2)
#   3. The message that should be printed(=$3)
#   4. Extra code to evaluate before exiting(=$4)
#
# If the message's log level is NOT error
#   2. The message that should be printed(=$2)
#
# LOG LEVELS
# verbose -> 1
# debug -> 2
# info -> 3
# warning -> 4
# error -> 5
#
# EXAMPLE
# log 3 "My info message"
# log 5 24 "My error message with exit code 24"
function log() {
  if [ "$logLevel" -gt "$1" ]; then
    return
  fi

  # verbose
  if [ "$1" -eq 1 ]; then
    echo -e "${TEXT_STYLE_FG_VERBOSE}VERBOSE:${TEXT_STYLE_END} $2"
    return
  fi

  # debug
  if [ "$1" -eq 2 ]; then
    return
  fi

  # info
  if [ "$1" -eq 3 ]; then
    echo -e "${TEXT_STYLE_FG_INFO}INFO:${TEXT_STYLE_END} $2"
    return
  fi

  # warning
  if [ "$1" -eq 4 ]; then
    echo -e "${TEXT_STYLE_FG_WARNING}WARNING:${TEXT_STYLE_END} $2"
    return
  fi

  # error
  if [ "$1" -eq 5 ]; then
    echo -e "${TEXT_STYLE_FG_ERROR}ERROR!${TEXT_STYLE_END}"
    echo -e "$3"
    echo

    eval "$4"

    exit "$2"
  fi
}

# Print info at the start of the script.
function info() {
  echo "$PROGRAM_NAME v$VERSION"
  echo "Author: $AUTHOR"
  echo "Report Email: $REPORT_EMAIL"
  echo
}

function help() {
  echo "Help"
  exit 0
}

function fromTTF() {
  local encoding="T1-WGL4.enc"

  local font=$1
  local fontExt=$2
  local fontFileName=$3
  local fontFilesDir=$4
  local outputDir=$5
  local series=$6
  local shape=$7

  cp -t "$fontFilesDir" "$font"

  cp -t "$outputDir" "$font"
  local newFontFile="$outputDir/$fontFileName.$fontExt"

  local outFile
  outFile=$(mktemp)

  ttf2tfm "$newFontFile" -p "$encoding" &>"$outFile"
  status=$?

  if [ 0 -ne "$status" ]; then
    log 5 101 "$(cat "$outFile")" ""
  fi

  rm -f "$newFontFile"

  echo "font: $font"
  echo "fontExt: $fontExt"
  echo "fontFileName: $fontFileName"
  echo "fontFilesDir: $fontFilesDir"
  echo "series: $series"
  echo "shape: $shape"
  echo
}

######################## Script ########################

info

readonly OUTPUT_DIR=$1

# Create $OUTPUT_DIR if it does not exist.
if [ -d "$OUTPUT_DIR" ]; then
  rm -rf "$OUTPUT_DIR"
fi
mkdir -p "$OUTPUT_DIR"

# Skip first argument because it is used for the $OUTPUT_DIR .
args=("$@")
args=("${args[@]:1}")
readonly argsLength=${#args[@]}

# No font arguments passed
if [ -z "${args[*]}" ]; then
  help
fi

# Create a directory for the actual font files.
readonly FONT_FILES_DIR="$OUTPUT_DIR/font"
mkdir -p "$FONT_FILES_DIR"

cd "$OUTPUT_DIR" || log 5 1 "Failed to navigate into '$OUTPUT_DIR'."

for ((i = 0; i < argsLength; i++)); do
  IFS=":" read -ra parts <<<"${args[$i]}"

  font=${parts[0]}
  series=${parts[1]}
  shape=${parts[2]}

  # Retrieve the extension from the $font file.
  IFS="." read -ra fontParts <<<"$font"
  fontPartsLength=${#fontParts[@]}
  fontExt="${fontParts[((fontPartsLength - 1))]}"

  fontFileName=${font/%".$fontExt"/""}
  fontFileName=$(basename "$fontFileName")

  # For now only ttf is a valid file format.
  case $fontExt in
  ttf) fromTTF "$font" "$fontExt" "$fontFileName" "$FONT_FILES_DIR" "$OUTPUT_DIR" "$series" "$shape" ;;
  *) log 5 2 "The font format '$fontExt' is not supported." "rm -rf $OUTPUT_DIR" ;;
  esac

done

exit 0
