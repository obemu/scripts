#! /bin/bash

# Script for simple usage of the brscan-skey tool
# Copyright (C) 2022 Emanuel Oberholzer

# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.

# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.

# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

######################## Constants ########################

readonly REPORT_EMAIL="emanueloberholzer400@gmail.com"
readonly AUTHOR="Emanuel Oberholzer"

readonly PROGRAM_NAME="create_latex_font"
readonly VERSION="1.0.0"

# https://en.wikipedia.org/wiki/ANSI_escape_code#Colors
readonly TEXT_STYLE_END="\x1B[0m"
readonly TEXT_STYLE_BOLD="\x1B[1;37m"
readonly TEXT_STYLE_FG_ERROR="\x1B[38;5;196m"
readonly TEXT_STYLE_FG_WARNING="\x1B[4;38;5;208m"
readonly TEXT_STYLE_FG_INFO="\x1B[38;5;46m"
readonly TEXT_STYLE_FG_VERBOSE=$TEXT_STYLE_BOLD

# Used in function log()
logLevel=1

######################## Functions ########################

# Print a log message to stdout.
#
# PARAMETERS
#   1. The log level of the message(=$1)
#
# If the message's log level is error
#   2. Exit code for the script(=$2)
#   3. The message that should be printed(=$3)
#
# If the message's log level is NOT error
#   2. The message that should be printed(=$2)
#
# LOG LEVELS
# verbose -> 1
# debug -> 2
# info -> 3
# warning -> 4
# error -> 5
#
# EXAMPLE
# log 3 "My info message"
# log 5 24 "My error message with exit code 24"
function log() {
  if [ "$logLevel" -gt "$1" ]; then
    return
  fi

  # verbose
  if [ "$1" -eq 1 ]; then
    echo -e "${TEXT_STYLE_FG_VERBOSE}VERBOSE:${TEXT_STYLE_END} $2"
    return
  fi

  # debug
  if [ "$1" -eq 2 ]; then
    return
  fi

  # info
  if [ "$1" -eq 3 ]; then
    echo -e "${TEXT_STYLE_FG_INFO}INFO:${TEXT_STYLE_END} $2"
    return
  fi

  # warning
  if [ "$1" -eq 4 ]; then
    echo -e "${TEXT_STYLE_FG_WARNING}WARNING:${TEXT_STYLE_END} $2"
    return
  fi

  # error
  if [ "$1" -eq 5 ]; then
    echo -e "${TEXT_STYLE_FG_ERROR}ERROR!${TEXT_STYLE_END}"
    echo -e "$3"
    echo

    exit "$2"
  fi
}

# Print info at the start of the script.
function info() {
  echo "$PROGRAM_NAME v$VERSION"
  echo "Author: $AUTHOR"
  echo "Report Email: $REPORT_EMAIL"
  echo
}

function help() {
  echo "This script generates a font family definition file from a "
  echo "provided input font file, which you then can use in your LaTeX "
  echo "documents."
  echo
  echo

  echo "The 1.parameter is the path to the input file (eg.: foo/bar/font.ttf)."
  echo
  echo "The 2.parameter is the path to the output directory, it defaults to the "
  echo "parent directory of the input file (eg.: foo/bar)."
  echo
  echo

  echo "See https://tug.org/TUGboat/Articles/tb27-1/tb86kroonenberg-fonts.pdf"
  echo

  exit 0
}

# Create the necessary files to use a .ttf in a LaTeX document with pdftex.
#
# PARAMETERS
#   1. output directory
#   2. input file
#   3. input file name
function fromTTF() {
  local outputDir=$1
  local inputFile=$2
  local inputFileName=$3

  # shellcheck disable=SC2164
  cd "$outputDir"

  readonly ENCODED_INPUT_FILE_NAME="$inputFileName-8y"

  ttf2afm "$inputFile" >"$inputFileName.afm"
  afm2pl -p texnansi "$inputFileName" "$ENCODED_INPUT_FILE_NAME"

  # Replace .pfb with .ttf in generated .map file from afm2pl
  sed -i "s/.pfb/.ttf/" "$ENCODED_INPUT_FILE_NAME.map"

  pltotf "$ENCODED_INPUT_FILE_NAME" &>/dev/null

  readonly DEFINITION_FILE="ly1-$inputFileName.fd"

  echo "\ProvidesFile{$DEFINITION_FILE}
\DeclacreFontFamily{LY1}{$inputFileName}{}
\DeclacreFontFamily{LY1}{$inputFileName}{m}{n}{ <-> $ENCODED_INPUT_FILE_NAME }{}
" >"$DEFINITION_FILE"
}

######################## Script ########################

info

if [ -z "$1" ] && [ -z "$2" ]; then
  help
fi

readonly INPUT_FILE_PATH=$1
outputDir=$2

if [ -z "$INPUT_FILE_PATH" ]; then
  log 5 1 "You have to provide an input ttf file."
fi

if ! [ -e "$INPUT_FILE_PATH" ]; then
  log 5 2 "The input file '$TTF_FILE' does not exist."
fi

readonly INPUT_FILE=${INPUT_FILE_PATH##*/}
readonly INPUT_FILE_DIR=${INPUT_FILE_PATH%/*}

# If outputDir does not exist, or is not provided use
# INPUT_FILE_DIR.
if [ -z "$outputDir" ] || ! [ -d "$outputDir" ]; then
  outputDir=$INPUT_FILE_DIR
  pwd="$(pwd)"

  cd /
  if ! [ -d "$outputDir" ]; then
    outputDir="$pwd/$outputDir"
  fi

  # pwd always exists
  # shellcheck disable=SC2164
  cd "$pwd"

  log 3 "Using '$outputDir' as output directory, because no valid directory was provided."
fi

IFS="." read -ra inputFileParts <<<"$INPUT_FILE"
readonly INPUT_FILE_NAME=${inputFileParts[0]}
readonly INPUT_FILE_EXT=${inputFileParts[1]}

# Only ttf supported for now
if [ "ttf" != "$INPUT_FILE_EXT" ]; then
  log 5 3 "This script only supports ttf input files."
fi

fromTTF "$outputDir" "$INPUT_FILE" "$INPUT_FILE_NAME"

exit 0
