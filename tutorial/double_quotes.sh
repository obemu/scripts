#! /bin/bash

# Declare bash string variable
bashVar="Bash Script"

# echo content of variable bashVar
echo $bashVar

# meta characters and its special meaning in bash is 
# suppressed when using double quotes except "$", "\" and "`"
echo "It's $bashVar and \"$bashVar\" using backticks: `date`" 
exit 0
