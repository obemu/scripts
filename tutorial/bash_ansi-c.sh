#! /bin/bash

# as an example we have used \n as a new line, \x40 is the
# hex value for @, and the octal value for .
echo $'web: www.linuxconfig.org\nemail: web\x40linuxconfigorg'
