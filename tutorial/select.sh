#! /bin/bash

# PS3 – Prompt used by “select” inside shell script
# https://www.thegeekstuff.com/2008/09/bash-shell-take-control-of-ps1-ps2-ps3-ps4-and-prompt_command/
PS3='Choose one word: '

# bash select
select word in "linux" "bash" "scripting" "tutorial"; do
  echo "The word you have selected is: $word"
  # Break, otherwise endless loop
  break
done

exit 0
