#! /bin/bash

# Declare bash string variable
BASH_VAR="Bash script"

# echo variable BASH_VAR
echo $BASH_VAR

# when meta character such as "$" is escaped with "\"
# it will be read literally
echo \$BASH_VAR

# backslash also has special meaning and it can be supressed with yet another "\"
echo "\\"
