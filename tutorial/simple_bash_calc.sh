#! /bin/bash

# Simple linux bash calculator
echo "Enter input: "
read userInput

echo "Result with 2 digits after decimal point:"
echo "scale=2; $userInput" | bc

echo "Result with 10 digits after decimal point:"
echo "scale=10; $userInput" | bc

echo "Result as rounded integer:"
echo "$userInput" | bc

exit 0
