#! /bin/bash

# Bash functions can be declared in any order

function funB() {
  echo Function B
}

function funA() {
  echo $1
}

function funD() {
  echo Function D
}

function funC() {
  echo Fun C: $1
}

# function calls
# pass parameter to funA
funA "Function A"
funB
# Pass parameter to function C
funC "In Function C"
funD
