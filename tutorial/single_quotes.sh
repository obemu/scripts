#! /bin/bash

# Declare bash string variable
bashVar="Bash Script"

# echo content of variable bashVar
echo $bashVar

# meta characters special meaning in bash is suppressed when  using single quotes
echo '$bashVar "$bashVaar"'

exit 0
