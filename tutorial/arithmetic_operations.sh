#! /bin/bash

echo '### let ###'

# bash addition
let addition=3+5
echo "3 + 5 =" $addition

# bash substraction
let substraction=7-8
echo "7 - 8 =" $substraction

# bash multiplication
let multiplication=5*8
echo "5 * 8 =" $multiplication

# bash division
let division=4/2
echo "4 / 2 =" $division

# bash modulus
let modulus=9%4
echo "9 % 4 =" $modulus

# bash power
let power=2**2
echo "2 ^ 2 =" $power



echo '### Bash Arithmetic Expansion ###'
# There are two formats for arithmetic expansion:
# POSIX $[ expression ] and non-POSIX $(( expression ))
# its your joice which you use
# https://unix.stackexchange.com/a/306115

echo '3 + 8 =' $[ 3 + 8 ]
echo '3 - 8 =' $((3-8))
echo '3 * 8 =' $[ 3 * 8 ]
echo '3 / 8 =' $((3/8))
echo '3 % 8 =' $[ 3 % 8 ]
echo '3 ** 3 =' $((3**3))



echo '### Declare ###'

echo -e "Please enter two numbers \c"
# read user input
read num1 num2
declare -i result
result=$num1+$num2
echo "Result is: $result"

# bash convert binary number 0b10001
result=2#10001
echo "Converted binary 0b10001: $result"

# bash convert octal number 016
result=8#16
echo "Converted octal 016: $result"

# bash convert hex number 0xE6A
result=16#E6A
echo "Converted hex 0xE6A: $result"

exit 0