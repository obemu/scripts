#! /bin/bash

directory="./"

# check if directory exists
if [ -d $directory ]; then
  echo "Directory \"$directory\" exists"
else
  echo "Directory \"$directory\" does not exist"
fi