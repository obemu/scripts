#! /bin/bash

# Declare array with 4 elements
ARRAY=('Debian Linux' 'Redhat Linux' Ubuntu Linux)

# get number of elements in the array
ELEMENTS=${#ARRAY[@]}

# echo each element in array
for ((i = 0; i < $ELEMENTS; i++)); do
  echo ${ARRAY[$i]}
done

# Empty line break
echo

# echo each element in array
for i in $(seq 0 ${ELEMENTS-1}); do
  echo ${ARRAY[$i]}
done

# Empty line break
echo

# echo each element in array
# This loop thinks
# ARRAY = (Debian Linux)
# because 'Debian Linux' was the first element
# wrapped in '' 
for item in $ARRAY; do
  echo "$item"
done
