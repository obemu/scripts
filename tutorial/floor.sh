#! /bin/bash

float=3.3446
echo "Float is: $float"

# floor floating point number
for rounded in $(printf %.0f $float); do
  echo "Floored number with bash: $rounded"
done

exit 0
