#! /bin/bash

file="./assets/input.txt"

if [ -e $file ]; then
  echo "$file exists!"
else
  echo "$file does not exist!"
fi

while [ ! -e $file ]; do
  # Sleep until file does exist/is created
  sleep 1
done
