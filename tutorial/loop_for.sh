#! /bin/bash

# bash for loop
# shellcheck disable=SC2045
for f in $(ls /var/); do
  echo $f
done

# Print all args from command line
# shellcheck disable=SC2068
for arg in $@; do
  echo $arg
done

for ((i = 0; i < $count; i++)); do
  echo $i
done
