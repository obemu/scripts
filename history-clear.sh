#! /bin/bash

# See https://askubuntu.com/a/496107

# Clear current shell history
history -c

# Overwrite $HISTFILE
history -w

exit 0