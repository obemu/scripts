#! /bin/bash

file=$1
start=$2
end=$3
outFile="cut.$file"

ffmpeg -v quiet -ss $start -i "$file" -c copy -t $end "$outFile"

exit 0
