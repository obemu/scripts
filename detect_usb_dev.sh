#! /bin/bash

# shellcheck disable=SC2010

dir=$(mktemp -d)
before="$dir/before"
after="$dir/after"

if ! [ -d "$dir" ]; then
  echo "Could not create a temporary directory with 'mktemp'."
  exit 1
fi

echo -n "Disconnect USB device. Then press ENTER"
read -r
ls -p1 /dev/ | grep -v / >"$before"

echo "Now connect USB device. Then press ENTER"
read -r
ls -p1 /dev/ | grep -v / >"$after"

difference=$(diff "$before" "$after")

if [ -z "$difference" ]; then
  echo "No new USB device detected."
else
  echo "$difference"
fi

exit 0
