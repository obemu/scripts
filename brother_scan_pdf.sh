#! /bin/bash

# Script for simple usage of the brscan-skey tool
# Copyright (C) 2022 Emanuel Oberholzer

# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.

# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.

# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

###########################################################

# This script uses the brscan-skey tool to scan a document from a brother printer.
# To print all options of the brscan-skey tool use the following command:
# brscan-skey -h 2
#
# The exact drivers/tools that were used to develop this script were downloaded
# from the following website:
#
# https://www.brother.at/support/mfc-9142cdn/downloads
#
# Install script:
# https://support.brother.com/g/b/branch/downloadend.aspx?c=de&lang=de&prod=mfc9142cdn_eu&os=128&dlid=dlf006893_000&flang=4&type3=625&dlang=true
#
# LPR printer driver (deb package):
# https://support.brother.com/g/b/branch/downloadend.aspx?c=de&lang=de&prod=mfc9142cdn_eu&os=128&dlid=dlf101616_000&flang=4&type3=559
#
# CUPSwrapper printer driver (deb package):
# https://support.brother.com/g/b/branch/downloadend.aspx?c=de&lang=de&prod=mfc9142cdn_eu&os=128&dlid=dlf101617_000&flang=4&type3=561

######################## Constants ########################

readonly REPORT_EMAIL="emanueloberholzer400@gmail.com"
readonly AUTHOR="Emanuel Oberholzer"

readonly PROGNAME="brother_scan_pdf"
readonly VERSION="1.0.7"

# https://en.wikipedia.org/wiki/ANSI_escape_code#Colors
readonly ANSI_END="\x1B[0m"
readonly ANSI_BOLD="\x1B[1;37m"
readonly ANSI_FG_RED="\x1B[38;5;196m"
readonly TEXT_STYLE_FG_ERROR=$ANSI_FG_RED
readonly TEXT_STYLE_FG_WARNING="\x1B[4;38;5;208m"
readonly TEXT_STYLE_FG_INFO="\x1B[38;5;46m"
readonly TEXT_STYLE_FG_VERBOSE=$ANSI_BOLD

readonly BROTHER_SCAN_CONFIG_DIR="$HOME/.brscan-skey"
readonly BROTHER_DEFAULT_OUTPUT_DIR="$HOME/brscan"
readonly BROTHER_DEFAULT_SCAN_TO_FILE_CONFIG="/opt/brother/scanner/brscan-skey/scantofile.config"

readonly DEFAULT_SCAN_RESOLUTION=600
readonly DEFAULT_COMPRESSION_QUALITY=25

# Exit codes
readonly EXIT_SUCCESS=0
readonly EXIT_UNKNOWN_ARG=1
readonly EXIT_LIBTIFF_NOT_INSTALLED=2
readonly EXIT_COMPRESSION_QUALITY_OUT_OF_BOUNDS=3
readonly EXIT_OUTDIR_DOES_NOT_EXIST=4
readonly EXIT_FAILED_TO_CREATE_OUTDIR=5
readonly EXIT_EMPTY_BROTHER_SCAN_OUTDIR=6
readonly EXIT_NOT_A_TIFF_FILE=7
readonly EXIT_TIFF_TO_PDF_CONV_ERROR=8
readonly EXIT_ABORT_SCAN=9

readonly EXIT_COPY_FILE_ERROR=21

# Used in function log()
logLevel=1
FUNCTION_LOG_FILE=$(mktemp)
readonly FUNCTION_LOG_FILE

######################## Functions ########################

# Print a log message to stdout.
#
# PARAMETERS
#   1. The log level of the message(=$1)
#
# If the message's log level is error
#   2. Exit code for the script(=$2)
#   3. The message that should be printed(=$3)
#
# If the message's log level is NOT error
#   2. The message that should be printed(=$2)
#
# LOG LEVELS
# verbose -> 1
# debug -> 2
# info -> 3
# warning -> 4
# error -> 5
#
# EXAMPLE
# log 3 "My info message"
# log 5 24 "My error message with exit code 24"
function log() {
  if [ "$logLevel" -gt "$1" ]; then
    return
  fi

  # verbose
  if [ "$1" -eq 1 ]; then
    echo -e "${TEXT_STYLE_FG_VERBOSE}VERBOSE:${ANSI_END} $2"
    return
  fi

  # debug
  if [ "$1" -eq 2 ]; then
    echo -e "DEBUG: $2"
    return
  fi

  # info
  if [ "$1" -eq 3 ]; then
    echo -e "${TEXT_STYLE_FG_INFO}INFO:${ANSI_END} $2"
    return
  fi

  # warning
  if [ "$1" -eq 4 ]; then
    echo -e "${TEXT_STYLE_FG_WARNING}WARNING:${ANSI_END} $2"
    return
  fi

  # error
  if [ "$1" -eq 5 ]; then
    echo -e "${TEXT_STYLE_FG_ERROR}ERROR!${ANSI_END}"
    echo -e "$3"
    echo

    exit "$2"
  fi
}

# Print info at the start of the script.
function info() {
  echo "$PROGNAME v$VERSION"
  echo "Author: $AUTHOR"
  echo "Report Email: $REPORT_EMAIL"
  echo
}

# Print help information and exit with code 0.
function help() {
  echo "${PROGNAME}"
  echo
  echo -e "${ANSI_FG_RED}NAME${ANSI_END}"
  echo -e "\t${PROGNAME} version ${VERSION}"
  echo
  echo -e "${ANSI_FG_RED}SYNOPSIS${ANSI_END}"
  echo -en "\t${ANSI_FG_RED}${PROGNAME}${ANSI_END} "
  echo -e "[ ${ANSI_FG_RED}<switches>...${ANSI_END} ]" # 2. argument
  echo
  echo -e "${ANSI_FG_RED}DESCRIPTION${ANSI_END}"
  echo -e "\tThis script can be used to scan a document into a pdf file."
  echo
  echo -e "${ANSI_FG_RED}SWITCHES${ANSI_END}"
  echo -en "\t${ANSI_FG_RED}-d, --default${ANSI_END}"
  echo -e "\tUse the default configuration options."
  echo
  echo -en "\t${ANSI_FG_RED}-t, --terminate${ANSI_END}"
  echo -e "\tTerminate the scanner tool after this script ends."
  echo
  echo -en "\t${ANSI_FG_RED}-h, --help${ANSI_END}"
  echo -e "\tPrint this message."
  echo
  echo -e "${ANSI_FG_RED}EXIT CODES${ANSI_END}"
  echo -e "\t${EXIT_SUCCESS}\t" \
    "The document has successfully been scanned and converted " \
    "to a pdf, or this help message was printed."
  echo
  echo -e "\t${EXIT_UNKNOWN_ARG}\t" \
    "An unknown argument was passed on the command line."
  echo
  echo -e "\t${EXIT_LIBTIFF_NOT_INSTALLED}\t" \
    "The package 'libtiff-tools' is not installed."
  echo
  echo -e "\t${EXIT_COMPRESSION_QUALITY_OUT_OF_BOUNDS}\t" \
    "The value that was passed for the compression quality " \
    "is out of bounds. Meaning it is either less than 1 or more than 100."
  echo
  echo -e "\t${EXIT_OUTDIR_DOES_NOT_EXIST}\t" \
    "The output directory for the scanned file does not exist and the " \
    "user does not want to create it."
  echo
  echo -e "\t${EXIT_FAILED_TO_CREATE_OUTDIR}\t" \
    "The output directory for the scanned file does not exist and " \
    "creating it failed."
  echo
  echo -e "\t${EXIT_EMPTY_BROTHER_SCAN_OUTDIR}\t" \
    "The scan process did not create a file."
  echo
  echo -e "\t${EXIT_NOT_A_TIFF_FILE}\t" \
    "The created file is not a '.tif' or '.tiff' file and " \
    "therefore cannot be converted to a PDF."
  echo
  echo -e "\t${EXIT_TIFF_TO_PDF_CONV_ERROR}\t" \
    "Converting the .tif/.tiff file to a PDF caused an error."
  echo
  echo -e "\t${EXIT_ABORT_SCAN}\t" \
    "The user aborted the scan process."
  echo
  echo -e "\t${EXIT_COPY_FILE_ERROR}\t" \
    "Copying a file caused an error."
  echo
  echo -e "${ANSI_FG_RED}AUTHOR${ANSI_END}"
  echo -e "\tWritten for Ubuntu by ${AUTHOR}."
  echo -e "\tReport bugs to: ${REPORT_EMAIL}"

  exit $EXIT_SUCCESS
}

# Copy the contents from src_file(=$1) to dst_file(=$2).
#
# dst_file is overwritten with the contents from src_file.
function copyFile() {
  if ! [ -e "$1" ]; then
    log 5 \
      $EXIT_COPY_FILE_ERROR \
      "Cannot copy the file '$1' to '$2', because the source file '$1' does not exist."
  fi

  if ! [ -e "$2" ]; then
    log 5 \
      $EXIT_COPY_FILE_ERROR \
      "Cannot copy the file '$1' to '$2', because the destination file '$2' does not exist."
  fi

  cat "$1" >"$2"
}

# Convert the tiff src_file(=$1) to a pdf file called dst_file(=$2)
#
# The compression quality(=$3).
# Should be within [1;100]
#
# Returns 0 if name(=$1) is installed, else some value other than 0.
#
# Example:
# convertToPdf "input.tiff" "output.pdf" 50
function convertToPdf() {
  local quality=$3
  if [ -z "$quality" ] || [ "$quality" -gt 100 ] || [ "$quality" -lt 1 ]; then
    echo "convertToPdf: Quality has to be within [1;100]. Quality is $quality." >"$FUNCTION_LOG_FILE"
    echo 1
    return
  fi

  allocLimit=$(getSizeInMiB "$1")

  # If an error occured, set $allocLimit to 0. This removes the limit entirely.
  if [ 0 -gt "$allocLimit" ]; then
    allocLimit=0
  fi

  # -j ... compress with JPEG
  # -q ... compression quality for JPEG
  # -m 123 ... memory allocation limit in MiB
  # -f ... Set PDF ``Fit Window'' user preference.
  # -o ... ouput file
  tiff2pdf "$1" -f -m "$allocLimit" -j -q "$quality" -o "$2" >"$FUNCTION_LOG_FILE" 2>&1
  echo $?
}

# Check if a package called name(=$1) is installed.
#
# Returns 0 if name(=$1) is installed, else some value other than 0.
#
# Example:
# isPackageInstalled gcc
function isPackageInstalled() {
  dpkg --no-pager -l "$1" >/dev/null 2>&1
  echo $?
}

# Get the size of file(=$1) in MiB.
#
# Returns -1 if an error occured.
function getSizeInMiB() {
  # Check if file exists.
  if ! [ -e "$1" ]; then
    echo "-1"
    return
  fi

  # Check if "du","cut" and "tr" are installed.
  # All of these programs are a part of the coreutils package.
  if [ 0 -ne "$(isPackageInstalled coreutils)" ]; then
    echo "-1"
    return
  fi

  du --block-size=MiB "$1" | cut -f1 | tr -d "\n" | tr -d "MiB"
}
######################## Script ########################

useDefaultCfg=false
shouldTerminate=false

# Parse all command line args and exit with $EXIT_UNKNOWN_ARG
# if an unrecognized arg is encountered.
for arg in "$@"; do
  case $arg in
  "-h" | "--help")
    help
    ;;

  "-d" | "--default")
    useDefaultCfg=true
    ;;

  "-t" | "--terminate")
    shouldTerminate=true
    ;;

  *)
    log 5 $EXIT_UNKNOWN_ARG "Unknown argument: $arg"
    ;;
  esac
done

info

# Check if libtiff-tools is installed.
if [ 0 -ne "$(isPackageInstalled libtiff-tools)" ]; then
  msg="The program 'tiff2pdf' from the package 'libtiff-tools' must be installed. "
  msg="${msg}This can be done by executing ${ANSI_BOLD}sudo apt-get update "
  msg="${msg}&& sudo apt-get install libtiff-tools${ANSI_END}."
  log 5 $EXIT_LIBTIFF_NOT_INSTALLED "$msg"
fi

# Check if default options should be used
if [ true = "$useDefaultCfg" ]; then
  log 1 "Using default options.\n"
  scanResolution=$DEFAULT_SCAN_RESOLUTION
  compressionQuality=$DEFAULT_COMPRESSION_QUALITY
  outputDir=$BROTHER_DEFAULT_OUTPUT_DIR
else
  ## Get scan resolution for the printer configuration.
  echo -en "Choose the scan resolution for your printer, or press "
  echo -en "${ANSI_BOLD}ENTER${ANSI_END} to use the default "
  echo -e "value. (Default is ${ANSI_BOLD}${DEFAULT_SCAN_RESOLUTION}${ANSI_END})"

  echo "1) 100"
  echo "2) 150"
  echo "3) 200"
  echo "4) 300"
  echo "5) 400"
  echo "6) 600"
  echo "7) 1200"
  echo "8) 2400"
  echo "9) 4800"
  echo "10) 9600"
  read -r chosenScanResolution

  case $chosenScanResolution in
  1)
    scanResolution=100
    ;;

  2)
    scanResolution=150
    ;;

  3)
    scanResolution=200
    ;;

  4)
    scanResolution=300
    ;;

  5)
    scanResolution=400
    ;;

  6)
    scanResolution=600
    ;;

  7)
    scanResolution=1200
    ;;

  8)
    scanResolution=2400
    ;;

  9)
    scanResolution=4800
    ;;

  10)
    scanResolution=9600
    ;;

  *)
    scanResolution=$DEFAULT_SCAN_RESOLUTION
    ;;
  esac

  # Print \n if a value was entered.
  if [ "$DEFAULT_SCAN_RESOLUTION" != "$scanResolution" ]; then
    echo
  fi

  ## Get compression quality for convertToPdf .
  echo -n "Enter the compression quality for the pdf file, or press "
  echo -en "${ANSI_BOLD}ENTER${ANSI_END} to use the default value. "
  echo -n "The value has to be within [1;100], a higher value equals better file quality, "
  echo -en "but also bigger file size. (Default is ${ANSI_BOLD}"
  echo -e "${DEFAULT_COMPRESSION_QUALITY}${ANSI_END}): "
  read -r compressionQuality

  # Use default or print \n if a value was entered.
  if [ -z "$compressionQuality" ]; then
    compressionQuality=$DEFAULT_COMPRESSION_QUALITY
  else
    echo
  fi

  # Check if compressionQuality is within the bounds [1;100]
  if [ "$compressionQuality" -gt 100 ] || [ "$compressionQuality" -lt 1 ]; then
    msg="Compression quality cannot be less than 1 or more than 100."
    msg="${msg}\nCompression quality: $compressionQuality"
    log 5 $EXIT_COMPRESSION_QUALITY_OUT_OF_BOUNDS "$msg"
  fi

  ## Get the output directory
  echo -n "Enter the output directory for scanned files, or press "
  echo -en "${ANSI_BOLD}ENTER${ANSI_END} to use the default value. "
  echo -e "(Default is '${ANSI_BOLD}${BROTHER_DEFAULT_OUTPUT_DIR}${ANSI_END}'): "
  read -r outputDir

  # Use default or print \n if a value was entered.
  if [ -z "$outputDir" ]; then
    outputDir=$BROTHER_DEFAULT_OUTPUT_DIR
  else
    echo
  fi
fi

# Check if outputDir actually exists and ask the user if he wants
# to create it if it does not exist
if ! [ -d "$outputDir" ]; then
  log 3 "The directory '$outputDir' does not exist, do you want to create it? [Y/n]: "
  read -r createOutputDir

  if [ "y" != "$createOutputDir" ] && [ "Y" != "$createOutputDir" ]; then
    log 5 \
      $EXIT_OUTDIR_DOES_NOT_EXIST \
      "Aborting, because '$outputDir' does not exist and cannot be created."
  fi

  if ! mkdir -p "$outputDir"; then
    log 5 \
      $EXIT_FAILED_TO_CREATE_OUTDIR \
      "Aborting, because '$outputDir' could not be created."
  fi
fi

## Get output filename
DEFAULT_OUTPUT_FILENAME="brscan_$(date +%Y-%m-%d-%H-%M-%S)"
readonly DEFAULT_OUTPUT_FILENAME

echo -n "Enter the desired name of the output file (.pdf gets appended), or press "
echo -en "${ANSI_BOLD}ENTER${ANSI_END} to use the default value. (Default "
echo -e "is '${ANSI_BOLD}${DEFAULT_OUTPUT_FILENAME}${ANSI_END}'): "
read -r outputFilename

# Use default or print \n if a value was entered.
if [ -z "$outputFilename" ]; then
  outputFilename=$DEFAULT_OUTPUT_FILENAME
else
  echo
fi

## Set output filepath
outputFilepath="$outputDir/$outputFilename.pdf"

# Print options
log 1 "Scanresolution is $scanResolution"
log 1 "Compression quality is $compressionQuality"
log 1 "Output filepath is $outputFilepath\n"

scanToFileConfigFile="$BROTHER_SCAN_CONFIG_DIR/scantofile.config"
scanToFileConfigExisted=false
if [ -e "$scanToFileConfigFile" ]; then
  log 3 "Found '$scanToFileConfigFile', creating backup."

  scanToFileConfigExisted=true

  # if scantofile.config exists, create a backup of it
  scanToFileConfigFileBackup=$(mktemp)
  copyFile "$scanToFileConfigFile" "$scanToFileConfigFileBackup"
else
  log 3 "Creating '$scanToFileConfigFile'."

  mkdir -p "$BROTHER_SCAN_CONFIG_DIR"
  touch "$scanToFileConfigFile"
  copyFile "$BROTHER_DEFAULT_SCAN_TO_FILE_CONFIG" "$scanToFileConfigFile"
fi

# Restore backup of original scantofile.config, if there is one
function restoreBackup() {
  if [ "true" = "$scanToFileConfigExisted" ]; then
    log 3 "Restoring backup for '$scanToFileConfigFile'."
    copyFile "$scanToFileConfigFileBackup" "$scanToFileConfigFile"
  fi
}

function trap_ctrlc() {
  restoreBackup
  exit $EXIT_ABORT_SCAN
}
function trap_terminate() {
  restoreBackup
  exit $EXIT_ABORT_SCAN
}

# Initialise trap to call trap_ctrlc function
# when signal 2 (SIGINT) is received.
trap "trap_ctrlc" 2
# Initialise trap to call trap_terminate function
# when signal 15 (SIGTERM) is received.
trap "trap_terminate" 15

# The following config options are available for brscan-skey,
# we are only interested in the resolution, so we do not change
# anything else.
#
# resolution=100,150,200,300,400,600,1200,2400,4800,9600
# source=FB,ADF_L,ADF_C
# duplex=OFF,ON
# size=MAX,A3,A4,A5,A6,Letter,Legal,${width}x${height} (mm)
sed -i "s/^resolution.*$/resolution=$scanResolution/" "$scanToFileConfigFile"

## Reload the new configurations.
brscan-skey
brscan-skey --refresh

## Wait for scan to finish
echo -en "Now scan your document.\nPress ${ANSI_BOLD}ENTER${ANSI_END} "
echo -en "once the scan has finished to continue, or ${ANSI_BOLD}ANY KEY"
echo -e "${ANSI_END} to abort."
read -n 1 -sr shouldContinue
echo

# This function gets executed, before the script exits.
function atExit() {
  if [ true = "$shouldTerminate" ]; then
    log 1 "Now terminating brscan-skey"
    brscan-skey -t
  fi

  restoreBackup
}

# If shouldContinue is NOT EMPTY then ENTER was NOT pressed.
if [ -n "$shouldContinue" ]; then
  atExit
  log 3 "Aborting."
  exit $EXIT_ABORT_SCAN
fi

## Get newest document in outputDir

# Scanned files are always put into $BROTHER_DEFAULT_OUTPUT_DIR,
# this cannot be changed.
mapfile -t documents < <(ls -1c "$BROTHER_DEFAULT_OUTPUT_DIR")

if [ 0 -eq ${#documents[@]} ]; then
  atExit
  log 5 \
    $EXIT_EMPTY_BROTHER_SCAN_OUTDIR \
    "The directory '$BROTHER_DEFAULT_OUTPUT_DIR' is empty."
fi

newest=${documents[0]}

# Split newest into its file name and its extension.
IFS=. read -ra newestParts <<<"$newest"
newestExt=${newestParts[1]}

if ! [ "tif" = "$newestExt" ] && ! [ "tiff" = "$newestExt" ]; then
  atExit
  log 5 \
    $EXIT_NOT_A_TIFF_FILE \
    "The scanned file has to end with '.tif' or '.tiff' ."
fi

## Convert to pdf
pdfDestinationFile="$outputFilepath"
pdfSourceFile="$BROTHER_DEFAULT_OUTPUT_DIR/$newest"
log 3 "Converting '$pdfSourceFile' to '$pdfDestinationFile' with quality=$compressionQuality..."
status=$(convertToPdf "$pdfSourceFile" "$pdfDestinationFile" $compressionQuality)
if [ 0 -ne "$status" ]; then
  atExit
  log 5 \
    $EXIT_TIFF_TO_PDF_CONV_ERROR \
    "$(cat "$FUNCTION_LOG_FILE")"
else
  log 3 "Successfully created '$pdfDestinationFile'."
fi

## Execute cleanup routine.
rm -f "$pdfSourceFile"
atExit

exit $EXIT_SUCCESS
