#! /bin/bash

# Check if this script is run as root.
if ([ 0 -ne $EUID ]); then
  echo "This script must be run as root."
  exit 1
fi

#  SUDO_UID        Set to the user ID of the user who invoked sudo
echo SUDO_UID: $SUDO_UID

#  SUDO_USER       Set to the login of the user who invoked sudo
echo SUDO_USER: $SUDO_USER

# runuser -l emanuel -c "touch ~/Downloads/test.txt"
runuser -l $SUDO_USER -c "touch ~/Downloads/test.txt"