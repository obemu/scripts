#! /bin/bash

codeExtensions="$HOME/.vscode/extensions"

# Loop through every file or directory in
# codeExtensions directory
extensions=($(ls $codeExtensions))
length=${#extensions[@]}
for ((i = 0; i < $length; i++)); do
  extension=${extensions[$i]}
  src="$codeExtensions/$extension"
  
  echo "[$((i + 1))/$length]: Copying \"$extension\" to \"$codiumExtensions\""
done

exit 0
