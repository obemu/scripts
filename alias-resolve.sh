#! /bin/bash

# shellcheck disable=SC2181
# shellcheck disable=SC1091

alias_name=$1

no_newline=false
if [ "-n" = "$alias_name" ]; then
  no_newline=true
  alias_name=$2
fi

if [ -z "$alias_name" ]; then
  echo "No alias name provided"
  exit 1
fi

source "$HOME/.bashrc" --always &>/dev/null

definition=$(alias "$alias_name" 2>/dev/null)
if [ 0 -ne $? ]; then
  echo "The alias '$alias_name' does not exist"
  exit 1
fi

resolved=$(echo -n "$definition" | cut -d"'" -f2)

if [ true = "$no_newline" ]; then
  echo "$resolved" | head -c-1
else
  echo "$resolved"
fi

exit 0
