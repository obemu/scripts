#! /bin/bash

# Event listener for gsettings
# Copyright (C) 2024 Emanuel Oberholzer
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

######################## About ########################

# This script listens to changes made in GSettings and executes user provided
# scripts whenever a subscribed value changes.

######################## Constants ########################

readonly PROGNAME="gsettings_monitor"
readonly VERSION="1.0.0"
readonly AUTHOR="Emanuel Oberholzer"

readonly PROGRAM_DIR="$HOME/.$PROGNAME"
readonly CONFIG_FILE="$PROGRAM_DIR/config.json"
readonly USER_EVENT_HANDLER_DIR="$PROGRAM_DIR/event_handler"

TEMP_DIR="$(mktemp -d)"
readonly TEMP_DIR
readonly TEMP_SETTINGS_CHANGELOGS_DIR="$TEMP_DIR/changelog"
readonly TEMP_EVENT_TRANSFORMER_DIR="$TEMP_DIR/transfomer"
readonly TEMP_EVENT_HANDLER_STDOUT_FILE="$TEMP_DIR/event_handler_stdout"
readonly TEMP_EVENT_HANDLER_STDERR_FILE="$TEMP_DIR/event_handler_stderr"

# https://en.wikipedia.org/wiki/ANSI_escape_code#Colors
readonly TEXT_STYLE_END="\x1B[0m"
readonly TEXT_STYLE_BOLD="\x1B[1;37m"
readonly TEXT_STYLE_FG_ERROR="\x1B[38;5;196m"
readonly TEXT_STYLE_FG_WARNING="\x1B[4;38;5;208m"
readonly TEXT_STYLE_FG_INFO="\x1B[38;5;46m"
readonly TEXT_STYLE_FG_DEBUG="\x1B[38;5;39m"
readonly TEXT_STYLE_FG_VERBOSE=$TEXT_STYLE_BOLD

readonly TEXT_STYLE_EVENT_HANDLER_STDOUT="\x1B[2;3;37m"
readonly TEXT_STYLE_EVENT_HANDLER_STDERR="\x1B[2;3;196m"

# Used in function _log()
g_log_level=1

# Global arrays that hold the process ids for all registered listeners.
declare -a g_gsettings_monitor_pids
declare -a g_settings_changelog_tail_pids
declare -i g_next_registered_listener_idx=0

# The PIDs for the current event handler and the processes that capture
# its STDOUT and STDERR.
# If there currently is no event handler, then these variables are set to -1.
declare g_current_event_handler_pid=-1
declare g_event_handler_stdout_tail_pid=-1
declare g_event_handler_stderr_tail_pid=-1

# Global variable that holds the contents of $CONFIG_FILE.
declare g_config

# Global flag that indicates whether or not the _dispose function has
# already been executed or not.
declare -i g_is_disposed=0

######################## Public Functions ########################

# Remove the surrounding characters of a string.
#
# PARAMETERS
#   1. The string with surrounding characters
#
# RETURNS
#   The string without surrounding characters, if there are any.
#
# EXAMPLE
#   remove_surrounding "'test'" -> "test"
#   remove_surrounding "<foo bar>" -> "foo bar"
#   remove_surrounding "<foo bar" -> "<foo bar"
function remove_surrounding() {
  local regex='^(.{1})(.*)\1$'

  # Return if the input does not have the same leading and trailing character.
  if ! echo -n "$1" | grep -E "$regex" >/dev/null; then
    echo "$1"
    return
  fi

  echo "$1" | sed -E "s/$regex/\\2/"
}

# Remove any surrounding square brackets from a string.
#
# PARAMETERS
#   1. The string that is surrounded by [...]
#
# RETURNS
#   The string without surrounding square brackets, if there are any.
#
# EXAMPLE
#   remove_surrounding_brackets "[test]" -> "test"
#   remove_surrounding_brackets "[foo bar]" -> "foo bar"
#   remove_surrounding_brackets "[foo bar" -> "[foo bar"
#   remove_surrounding_brackets "[foo    ->  "foo
#                                   bar]"       bar"
function remove_surrounding_brackets() {
  local str=$1
  local -r len="${#str}"
  local -i end_idx
  ((end_idx = len - 1))

  # Return early if the string has less than 2 characters, or
  # the first and last character are not squrare brackets.
  if [ "$len" -lt 2 ] || [ "${str:0:1}" != "[" ] || [ "${str:$end_idx:1}" != "]" ]; then
    echo "$str"
    return
  fi

  ((end_idx--))
  echo "${str:1:$end_idx}"
}

# Remove any leading and trailing whitespaces from the string.
#
# PARAMETERS
#   1. The string with optional leading or trailing whitespaces.
#
# RETURNS
#   The string without leading and trailing whitespaces.
#
# EXAMPLE
#   trim "  foo bar" -> "foo bar"
#   trim "  foo bar   " -> "foo bar"
#   trim "foo bar" -> "foo bar"
function trim() {
  # See https://stackoverflow.com/a/3232433
  echo "$1" | sed -E 's/^\s*//' | sed -E 's/\s*$//'
}

######################## Functions ########################

#	Print a log message to stdout.
#
#	PARAMETERS
#		1. The log level of the message(=$1)
#
#	If the message's log level is NOT error
#		2. The message that should be printed(=$2)
#
#	If the message's log level is error
#		2. Exit code for the script(=$2)
#		3. Extra code to evaluate before exiting(=$3)
#		4. The message that should be printed(=$4)
#
#	LEVELS
#
#		verbose -> 1
#		debug -> 2
#		info -> 3
#		warning -> 4
#		error -> 5
#
#	EXAMPLE
#
#		log 3 "My info message"
#
#		log 5 24 "" "My error message with exit code 24"
#
#		log 5 24 "ls -1" \
#			"My error message with exit code 24 and" \
#			"a list of all files in the current directory"
function _log() {
  if [ "$g_log_level" -gt "$1" ]; then
    return
  fi

  # verbose
  if [ "$1" -eq 1 ]; then
    shift
    echo -e "${TEXT_STYLE_FG_VERBOSE}VERBOSE:${TEXT_STYLE_END} $*"
    return
  fi

  # debug
  if [ "$1" -eq 2 ]; then
    shift
    echo -e "${TEXT_STYLE_FG_DEBUG}DEBUG:${TEXT_STYLE_END} $*"
    return
  fi

  # info
  if [ "$1" -eq 3 ]; then
    shift
    echo -e "${TEXT_STYLE_FG_INFO}INFO:${TEXT_STYLE_END} $*"
    return
  fi

  # warning
  if [ "$1" -eq 4 ]; then
    shift
    echo -e "${TEXT_STYLE_FG_WARNING}WARNING:${TEXT_STYLE_END} $*"
    return
  fi

  # error
  if [ "$1" -eq 5 ]; then
    local exit_code="$2"
    local eval_code="$3"

    shift 3

    echo -e "${TEXT_STYLE_FG_ERROR}ERROR!${TEXT_STYLE_END}"
    for msg in "$@"; do
      echo -ne "$msg"
    done
    echo

    eval "$eval_code"

    exit "$exit_code"
  fi
}

# Check if a given gsettings schema exists.
#
# PARAMETERS
#   1. The gsettings schema.
#
# EXIT CODE
#   0 if the schema exists, else 1.
function _gsettings_schema_exists() {
  gsettings list-keys "$1" >/dev/null 2>&1
}

# Check if a given key exists in a gsettings schema.
#
# PARAMETERS
#   1. The gsettings schema.
#   2. The key in the schema.
#
# EXIT CODE
#   0 if the key exists in the schema, else 1.
function _gsettings_schema_key_exists() {
  gsettings get "$1" "$2" >/dev/null 2>&1
}

# Check if a given key exists in a gsettings schema.
#
# PARAMETERS
#   1. The json object.
#   2. The name of the key.
#
# EXIT CODE
#   0 if the key exists in the schema, else 1.
function _jq_object_has_key() {
  local -r has_key="$(echo "$1" | jq "has(\"$2\")")"
  # jq 'has' function returns either 'true' or 'false'.
  [ "true" = "$has_key" ]
}

# Prettify the provided json object.
#
# PARAMETERS
#   1. The json object.
#
# RETURNS
#   A string with the prettified json object.
function _prettify_json() {
  echo "$1" | jq
}

# Get the filepath to the settings file that contains the changes for
# the specific setting.
#
# PARAMETERS
#   1. The gsettings schema.
#   2. The key in the schema.
#
# RETURNS
#   The path to the changelog file.
function _get_settings_changelog_filepath() {
  local schema="$1"
  local key="$2"

  if [ -n "$key" ]; then
    echo -n "$TEMP_SETTINGS_CHANGELOGS_DIR/$schema-$key"
  else
    echo -n "$TEMP_SETTINGS_CHANGELOGS_DIR/$schema"
  fi
}

# Register a listener for a specific setting.
#
# PARAMETERS
#   1. The gsettings schema.
#   2. The key in the schema (optional).
function _register_gsettings_listener() {
  local schema="$1"
  local key="$2"

  local -a args
  local -r outfile="$(_get_settings_changelog_filepath "$schema" "$key")"
  if [ -n "$key" ]; then
    _log 3 "Registering listener for setting '$key' in schema '$schema'"
    args=("$schema" "$key")
  else
    _log 3 "Registering listener for all settings in schema '$schema'"
    args=("$schema")
  fi

  ((registered_listener_idx = g_next_registered_listener_idx++))

  touch "$outfile"

  gsettings monitor "${args[@]}" >>"$outfile" &
  g_gsettings_monitor_pids[registered_listener_idx]=$!

  tail -f "$outfile" | while read -r line; do
    _on_setting_changed "$schema" "$line"
  done &
  g_settings_changelog_tail_pids[registered_listener_idx]=$!
}

# Kill a process by its ID, without printing any result or error messaged to
# stdout or stderr.
#
# PARAMETERS
#   1. The PID.
function _quiet_kill() {
  kill "$1" >/dev/null 2>&1
}

# Kill all registered listeners and remove all their changelogs.
function _dispose() {
  echo
  _log 3 "Removing all registered listeners"

  for ((i = 0; i < g_next_registered_listener_idx; i++)); do
    _quiet_kill "${g_gsettings_monitor_pids[$i]}"
    _quiet_kill "${g_settings_changelog_tail_pids[$i]}"
  done

  if [ "$g_current_event_handler_pid" -ge 0 ]; then
    _quiet_kill "$g_current_event_handler_pid"
    g_current_event_handler_pid=-1
  fi

  if [ "$g_event_handler_stdout_tail_pid" -ge 0 ]; then
    _quiet_kill "$g_event_handler_stdout_tail_pid"
    g_event_handler_stdout_tail_pid=-1
  fi

  if [ "$g_event_handler_stderr_tail_pid" -ge 0 ]; then
    _quiet_kill "$g_event_handler_stderr_tail_pid"
    g_event_handler_stderr_tail_pid=-1
  fi

  rm -rf "$TEMP_DIR"

  g_is_disposed=1
}

# Execute a jq filter on the contents of the global $g_config variable.
#
# PARAMETERS
#   1. The filter for jq.
#
# RETURNS
#   The result returned by jq.
function _query_config() {
  echo "$g_config" | jq "$1" -c
}

# Check if the process for the provided id is alive.
#
# PARAMETERS
#   1. The PID.
#
# EXIT CODE
#   0 if a process for the provided id is alive, else 1.
function _is_process_alive() {
  ps -p "$1" >/dev/null
}

# Execute and event handler and capture its output.
#
# PARAMETERS
#   1. The path to the event handler.
#   2. The gsettings schema for the event.
#   3. The gsettings schema key for the event.
#   4. The transformed gsettings schema value for the event.
function _exec_event_handler() {
  local -r handler="$1"
  local -r schema="$2"
  local -r key="$3"
  local -r value="$4"

  _log 1 "Executing event handler '$handler'"

  # Create files that hold the output
  touch "$TEMP_EVENT_HANDLER_STDOUT_FILE"
  touch "$TEMP_EVENT_HANDLER_STDERR_FILE"

  # Execute the event handler in a background process and capture its PID.
  "$handler" "$schema" "$key" "$value" 2>>"$TEMP_EVENT_HANDLER_STDERR_FILE" >>"$TEMP_EVENT_HANDLER_STDOUT_FILE" &
  g_current_event_handler_pid=$!

  # Forward lines written to STDOUT from the handler to _on_event_handler_stdout.
  tail -f "$TEMP_EVENT_HANDLER_STDOUT_FILE" | while read -r line; do
    _on_event_handler_stdout "$handler" "$schema" "$key" "$value" "$line"
  done &
  g_event_handler_stdout_tail_pid=$!

  # Forward lines written to STDERR from the handler to _on_event_handler_stderr.
  tail -f "$TEMP_EVENT_HANDLER_STDERR_FILE" | while read -r line; do
    _on_event_handler_stderr "$handler" "$schema" "$key" "$value" "$line"
  done &
  g_event_handler_stderr_tail_pid=$!

  # Wait until the event handler is finished.
  while _is_process_alive "$g_current_event_handler_pid"; do
    sleep 0.5
  done
  g_current_event_handler_pid=-1

  # Kill the process that forwards messages to _on_event_handler_stdout
  # and reset the file $TEMP_EVENT_HANDLER_STDOUT_FILE.
  _quiet_kill "$g_event_handler_stdout_tail_pid"
  g_event_handler_stdout_tail_pid=-1
  rm -f "$TEMP_EVENT_HANDLER_STDOUT_FILE"

  # Kill the process that forwards messages to _on_event_handler_stderr
  # and reset the file $TEMP_EVENT_HANDLER_STDERR_FILE.
  _quiet_kill "$g_event_handler_stderr_tail_pid"
  g_event_handler_stderr_tail_pid=-1
  rm -f "$TEMP_EVENT_HANDLER_STDERR_FILE"
}

# Execute and event handler and capture its output.
#
# PARAMETERS
#   1. The gsettings schema.
#   2. The gsettings schema key for the event.
#   3. The gsettings schema value for the event.
function _transform_value() {
  local -r schema="$1"
  local -r key="$2"
  local -r value="$3"

  local transformer

  # Check if there is a specific transformer for the given key.
  transformer="$(_query_config ".settings[\"$schema\"].keys[\"$key\"].transform")"

  if [ "null" = "$transformer" ]; then
    # Check if there is a generic transformer for the schema, If there
    # is none for the key
    transformer="$(_query_config ".settings[\"$schema\"].transform")"

    # Return the initial value if there still is no transformer
    if [ "null" = "$transformer" ]; then
      echo "$value"
      return
    fi
  fi

  local command
  command="$(echo "$transformer" | jq ".command")"

  # Exit with an error if there is no command.
  if [ "null" = "$command" ]; then
    _log 5 1 "_dispose" \
      "The transformer for the key '$key' in the schema '$schema' does " \
      "not have a command"
  fi

  local replace_str
  replace_str="$(
    echo "$transformer" | jq '.replace_str as $replace | if null == $replace then {} else $replace end'
  )"

  # Remove surrounding double quotes.
  command="$(remove_surrounding "$command")"
  replace_str="$(remove_surrounding "$replace_str")"

  # Replace "$GSETTINGS_MONITOR_PROGRAM_DIR" with the value of $PROGRAM_DIR
  command="${command//\$GSETTINGS_MONITOR_PROGRAM_DIR/$PROGRAM_DIR}"

  # Replace the $replace_str with $value
  command="${command//$replace_str/$value}"

  # Replace \" with "
  command="${command//\\\"/\"}"

  local -r command_err_file="$TEMP_EVENT_TRANSFORMER_DIR/$schema-$key.log"

  # Create the logfile, if is does not already exist.
  # Clear the file if it exists.
  echo -n >"$command_err_file"

  local -r transformed_value="$(eval "$command" 2>"$command_err_file")"
  local -r command_exit_code=$?

  # Print the error if the transformation failed.
  if [ 0 -ne $command_exit_code ]; then
    _log 5 1 "_dispose" \
      "The transformation for the value for the key '$key' in the " \
      "schema '$schema' failed.\n" \
      "------------------------------------------------\n" \
      "$(cat "$command_err_file")"
  fi

  echo "$transformed_value"
}

######################## Event listeners/handlers ########################

# Handles SIGINT and SIGTERM.
function _signal_handler() {
  _dispose
  exit 0
}

# Called whenever a setting is changed.
#
# PARAMETERS
#   1. The gsettings schema for the changed setting.
#   2. The event in the format '<key>: <value>'.
function _on_setting_changed() {
  local -r schema="$1"
  local -r event="$2"

  local -r regex='^([a-z-]+): (.+)$'

  # The key in the schema for the changed value.
  local -r key="$(echo "$event" | sed -E "s/$regex/\\1/")"

  # The changed value.
  local value
  value="$(echo "$event" | sed -E "s/$regex/\\2/")"

  _log 1 "Changed value for key '$key' in schema '$schema' to: $value"

  value="$(_transform_value "$schema" "$key" "$value")"

  # Execute general event handlers in order.
  find "$USER_EVENT_HANDLER_DIR" -maxdepth 1 -type f -executable | sort -n | while read -r handler; do
    _exec_event_handler "$handler" "$schema" "$key" "$value"
  done

  local -r schema_event_handler_dir="$USER_EVENT_HANDLER_DIR/$schema"

  # Return if the directory that should contain the event handlers does not exits.
  if ! [ -d "$schema_event_handler_dir" ]; then
    _log 4 \
      "At least one listener is registered for the schema '$schema' but " \
      "the directory ('$schema_event_handler_dir') that should contain the " \
      "event handlers for any changes in the setting does not exist."
    return
  fi

  # Execute schema specific event handlers in order.
  find "$schema_event_handler_dir" -maxdepth 1 -type f -executable | sort -n | while read -r handler; do
    _exec_event_handler "$handler" "$schema" "$key" "$value"
  done
}

# Called whenever an event handler writes a message to STDOUT.
#
# PARAMETERS
#   1. The path to the event handler.
#   2. The gsettings schema for the event.
#   3. The gsettings schema key for the event.
#   4. The transformed gsettings schema value for the event.
#   5. The message from the handler.
function _on_event_handler_stdout() {
  local -r handler="$1"
  local -r schema="$2"
  local -r key="$3"
  local -r value="$4"
  local -r message="$5"

  echo -e "\t${TEXT_STYLE_EVENT_HANDLER_STDOUT}${message}$TEXT_STYLE_END"
}

# Called whenever an event handler writes a message to STDERR.
#
# PARAMETERS
#   1. The path to the event handler.
#   2. The gsettings schema for the event.
#   3. The gsettings schema key for the event.
#   4. The transformed gsettings schema value for the event.
#   5. The message from the handler.
function _on_event_handler_stderr() {
  local -r handler="$1"
  local -r schema="$2"
  local -r key="$3"
  local -r value="$4"
  local -r message="$5"

  echo -e "\t${TEXT_STYLE_EVENT_HANDLER_STDERR}${message}$TEXT_STYLE_END" >&2
}

######################## Script ########################

echo "$PROGNAME v$VERSION"
echo "Author: $AUTHOR"
echo

# Create all necessary directories
mkdir -p "$USER_EVENT_HANDLER_DIR"
mkdir -p "$TEMP_SETTINGS_CHANGELOGS_DIR"
mkdir -p "$TEMP_EVENT_TRANSFORMER_DIR"

# Create an empty basic config file if none exists and then exit
if ! [ -e "$CONFIG_FILE" ]; then
  _log 3 \
    "Not registering any listeners, because no config file at '$CONFIG_FILE' exists."
  trim "
{
  \"settings\": []
}
" >"$CONFIG_FILE"
  exit 1
fi

# Read and store config data.
g_config="$(cat "$CONFIG_FILE")"

# Exit early if the settings object does not exist.
if ! _jq_object_has_key "$g_config" "settings"; then
  _log 5 1 "" \
    "The config file at '$CONFIG_FILE' does not contain the 'settings' " \
    "object, which defines which settings should be listened to."
fi

# Exit early if there are no settings that should be listened to.
settings_count="$(_query_config '.settings | length')"
if [ "$settings_count" -le 0 ]; then
  _log 3 \
    "Not registering any listeners, because the 'settings' object in " \
    "the config file at '$CONFIG_FILE' is empty."
  exit 0
fi

# Initialise trap to call '_signal_handler' function
# when signal 2 (SIGINT) or 15 (SIGTERM) is received.
trap "_signal_handler" 2
trap "_signal_handler" 15

# Export utility functions for usage in event handler scripts
export -f trim
export -f remove_surrounding
export -f remove_surrounding_brackets

# Register all listeners for each setting
mapfile -d ',' -t settings_schemas < <(remove_surrounding_brackets "$(_query_config ".settings | keys")")
for schema in "${settings_schemas[@]}"; do
  # Remove surrounding quotes.
  schema="$(remove_surrounding "$schema")"

  # Exit with an error if the schema does not exist.
  if ! _gsettings_schema_exists "$schema"; then
    _log 5 1 "_dispose" \
      "The gsettings schema '$schema' does not exist"
  fi

  # Get an array that contains every key that should be listened to
  # for this schema.
  raw_schema_keys="$(
    _query_config ".settings[\"$schema\"].keys as \$settings_keys | if null != \$settings_keys then \$settings_keys | keys else [] end"
  )"
  raw_schema_keys="$(remove_surrounding_brackets "$raw_schema_keys")"

  # If no keys are explicitely specified, then register a listener for the
  # entire schema and end the current loop.
  if [ -z "$raw_schema_keys" ]; then
    _register_gsettings_listener "$schema"
    continue
  fi

  # Add a listener for every specified key.
  mapfile -d ',' -t schema_keys < <(echo "$raw_schema_keys")
  for key in "${schema_keys[@]}"; do
    # Remove surrounding quotes.
    key="$(remove_surrounding "$key")"

    # Exit with an error if the key does not exist.
    if [ -n "$key" ] && ! _gsettings_schema_key_exists "$schema" "$key"; then
      _log 5 1 "_dispose" \
        "The key '$key' does not exist in the gsettings schema '$schema'"
    fi

    _register_gsettings_listener "$schema" "$key"
  done
done

# Loop while the program is not disposed.
while [ 0 -eq $g_is_disposed ]; do
  sleep 1
done
