#! /bin/bash

# Script for simple usage of the brscan-skey tool
# Copyright (C) 2022 Emanuel Oberholzer

# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.

# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.

# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

######################## Constants ########################

readonly REPORT_EMAIL="emanueloberholzer400@gmail.com"
readonly AUTHOR="Emanuel Oberholzer"

readonly PROGRAM_NAME="gen_pw"
readonly VERSION="1.0.0"

# https://en.wikipedia.org/wiki/ANSI_escape_code#Colors
readonly TEXT_STYLE_END="\x1B[0m"
readonly TEXT_STYLE_BOLD="\x1B[1;37m"
readonly TEXT_STYLE_FG_ERROR="\x1B[38;5;196m"
readonly TEXT_STYLE_FG_WARNING="\x1B[4;38;5;208m"
readonly TEXT_STYLE_FG_INFO="\x1B[38;5;46m"
readonly TEXT_STYLE_FG_VERBOSE=$TEXT_STYLE_BOLD

DEFAULT_SHA_COUNT=6
DEFAULT_LENGTH=16

# Used in function log()
logLevel=1

######################## Functions ########################

# Print a log message to stdout.
#
# PARAMETERS
#   1. The log level of the message(=$1)
#
# If the message's log level is error
#   2. Exit code for the script(=$2)
#   3. The message that should be printed(=$3)
#   4. Extra code to evaluate before exiting(=$4)
#
# If the message's log level is NOT error
#   2. The message that should be printed(=$2)
#
# LOG LEVELS
# verbose -> 1
# debug -> 2
# info -> 3
# warning -> 4
# error -> 5
#
# EXAMPLE
# log 3 "My info message"
# log 5 24 "My error message with exit code 24"
function log() {
  if [ "$logLevel" -gt "$1" ]; then
    return
  fi

  # verbose
  if [ "$1" -eq 1 ]; then
    echo -e "${TEXT_STYLE_FG_VERBOSE}VERBOSE:${TEXT_STYLE_END} $2"
    return
  fi

  # debug
  if [ "$1" -eq 2 ]; then
    return
  fi

  # info
  if [ "$1" -eq 3 ]; then
    echo -e "${TEXT_STYLE_FG_INFO}INFO:${TEXT_STYLE_END} $2"
    return
  fi

  # warning
  if [ "$1" -eq 4 ]; then
    echo -e "${TEXT_STYLE_FG_WARNING}WARNING:${TEXT_STYLE_END} $2"
    return
  fi

  # error
  if [ "$1" -eq 5 ]; then
    echo -e "${TEXT_STYLE_FG_ERROR}ERROR!${TEXT_STYLE_END}"
    echo -e "$3"
    echo

    eval "$4"

    exit "$2"
  fi
}

# Print info at the start of the script.
function info() {
  echo "$PROGRAM_NAME v$VERSION"
  echo "Author: $AUTHOR"
  echo "Report Email: $REPORT_EMAIL"
  echo
}

function help() {
  info

  echo "Generate a random password"
  echo
  echo "Usage: gen_pw [SHA_COUNT] [LENGTH]"
  echo "Usage: gen_pw -h | --help"
  echo
  echo "SHA_COUNT: How many times the sha512sum algorithm should be used."
  echo "LENGTH: The length of the generated password."
  echo
  echo "-h | --help: Print this message."
  echo

  exit 0
}

# Use random sha algorithms to get a valu
#
# PARAMETERS
#   1. How many times the sha algo should be used.
#   2. The initial number
function getSha() {
  local count=$1
  local number=$2

  for ((i = 0; i < count; i++)); do
    number=$(echo "$number" | sha512sum)

    # remove last 3 chars "  -"
    len=${#number}
    number=${number:0:((len - 3))}
  done

  echo "$number"
}

# Check if string(=$1) is a number
#
# Returns "true" if string is a number, else "false".
function isNumber() {
  regex='^[0-9]+$'

  if ! [[ $1 =~ $regex ]]; then
    echo "false"
    return
  fi

  echo "true"
}

######################## SCRIPT ########################

if [ "--help" = "$1" ] || [ "-h" = "$1" ]; then
  help
fi

shaCount=$1
if [ -z "$shaCount" ]; then
  shaCount=$DEFAULT_SHA_COUNT
fi
if [ "false" = "$(isNumber $shaCount)" ]; then
  log 5 1 "The value for SHA_COUNT has to be a number, but it is '$shaCount'."
fi

length=$2
if [ -z "$length" ]; then
  length=$DEFAULT_LENGTH
fi
if [ "false" = "$(isNumber $length)" ]; then
  log 5 1 "The value for LENGTH has to be a number, but it is '$shaCount'."
fi

# Get a random file from the binary directory
dir="/$(readlink /bin)"
mapfile -t files < <(find "$dir" -type f)
filesLength=${#files[@]}

nanos=$(date +%N)
index=$(rand -u -s "$nanos" -M "$filesLength")
input=$(tr -d '\0' <"${files[$index]}")

sha=$(getSha "$shaCount" "$input")
while [ $length -gt ${#sha} ]; do
  sha="$sha$(getSha "$shaCount" "$sha")"
done

pw=${sha:0:$length}

echo "$pw"

exit 0
