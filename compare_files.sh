#! /bin/bash

# shellcheck disable=SC2086

file=/home/emanuel/Projects/Scripts/out

grep -v '^ *#' <$file | while IFS= read -r line; do
  IFS=":" read -r -a files <<<"$line"
  echo -e "${files[0]}\n${files[1]}\n"

  diff -a --color ${files[0]} ${files[1]}
  echo -e "\n"

  # Wait for user to continue
  echo -e "Continue (y/n)? \c"
  read -r -n 1 input <&1
  echo

  if [ "y" != "$input" ]; then
    exit 0
  fi

done

echo "Finished!"

exit 0
