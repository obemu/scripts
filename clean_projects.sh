#! /bin/bash

# Copyright (C) 2023 Emanuel Oberholzer

# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.

# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.

# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

###### Shellchecks ######

# shellcheck disable=SC2181

###### Constants ######

readonly PROJECTS_DIR="$HOME/projects"

###### Functions ######

# Set filetype(=$1) to "d" if you want to look
# for directories and to "f" if you want to look
# for files in the current working directory.
#
# Set the name(=$2) of the file/dir to remove.
function removeFiles() {
  # Check if filetype is either "d" or "f".
  if [ "d" = "$1" ]; then
    filetype="directoris"
  elif [ "f" = "$1" ]; then
    filetype="files"
  else
    echo "Filetype has to be 'd' or 'f'"
    exit 1
  fi

  # Check if name is an empty string.
  if [ -z "$2" ]; then
    echo "Not provided a filename"
    exit 2
  fi

  removedAnything=false

  # Find all files called $2 and remove them.
  while read -r fl; do
    removedAnything=true
    rm -rf "$fl"
  done < <(find "$PROJECTS_DIR" -type "$1" -name "$2" 2>/dev/null)

  # Check if any files where removed at all.
  if [ "false" = "$removedAnything" ]; then
    echo "There are no $filetype called '$2'"
    return
  fi

  # Print exit status.
  if [ 0 -eq $? ]; then
    echo "Successfully removed all $filetype called '$2'"
  else
    echo "Failed to remove all $filetype called '$2'"
  fi
}

###### Script ######

echo -e "Projects directory is: '$PROJECTS_DIR'\n"

# Clean Dart projects.
removeFiles "d" ".dart_tool"

# Clean Python projects.
removeFiles "d" "__pycache__"

# Clean C/CPP build directories.
removeFiles "d" "build"
