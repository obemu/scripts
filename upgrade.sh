#! /bin/bash

mask=$1
if [ -z "$mask" ]; then
  mask=2
fi

# Get flags from bitmask
((checkRoot = mask & 0x01))
((useApt = mask & 0x02))
((useSnap = mask & 0x04))
((useFlatpak = mask & 0x08))

# Check if this script is run as root.
if [ 0 -ne $checkRoot ] && [ 0 -ne $EUID ]; then
  echo "This script must be run as root."
  exit 1
fi

# Get the real username of the user who started this script.
# We need to do this, becauser $USER just returns "root", because
# this script needs to be run as root.
readonly realuser="${SUDO_USER:-USER}"

if [ 0 -ne $useApt ]; then
  echo
  echo "_______UPDATE_______"
  apt update

  echo
  echo "_______LIST UPGRADEABLE_______"
  apt list --upgradeable -a

  echo
  echo "_______UPGRADE_______"
  apt upgrade -y

  echo
  echo "_______AUTOREMOVE_______"
  apt autoremove -y
fi

if [ 0 -ne $useSnap ]; then
  echo
  echo "_______SNAP REFRESH_______"
  snap refresh
fi

if [ 0 -ne $useFlatpak ]; then
  echo
  echo "_______FLATPAK UPDATE_______"
  sudo -u "$realuser" flatpak update -y
fi

exit 0
