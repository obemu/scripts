#! /bin/bash

# Script for mounting/unmounting the disk images in assets/img
# Emanuel Oberholzer (C) 2023

# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.

# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.

# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

######################## DISABLED SHELLCHECKS ########################

# shellcheck disable=SC2155

######################## CONSTANTS ########################

readonly REPORT_EMAIL="emanueloberholzer400@gmail.com"
readonly AUTHOR="Emanuel Oberholzer"

readonly PROGNAME="mount_utils"
readonly VERSION="0.0.1"

# https://en.wikipedia.org/wiki/ANSI_escape_code#Colors
readonly ANSI_END="\x1B[0m"
readonly ANSI_BOLD="\x1B[1;37m"
readonly ANSI_FG_RED="\x1B[38;5;196m"

# Styles for log function
readonly TEXT_STYLE_ERROR=$ANSI_FG_RED
readonly TEXT_STYLE_WARNING="\x1B[4;38;5;208m"
readonly TEXT_STYLE_INFO="\x1B[38;5;46m"
readonly TEXT_STYLE_VERBOSE=$ANSI_BOLD

SCRIPT_PATH="$(readlink -e "$0")"
declare -r SCRIPT_PATH

readonly TRUE="true"
readonly FALSE="false"

# Exit codes
readonly EXIT_SUCCESS=0
readonly EXIT_UNKNOWN_ARG=1
readonly EXIT_NO_VALUE_FOR_ARG=2
readonly EXIT_IMG_PATH_DOES_NOT_EXIST=3
readonly EXIT_NO_PARTITIONS=4
readonly EXIT_FAILED_TO_CREATE_MOUNTPOINT=5
readonly EXIT_NO_IMG_IN_SQUASH=6
readonly EXIT_SQUASH_IMG_ALREADY_MOUNTED=7
readonly EXIT_PARTITION_ALREADY_MOUNTED=8

readonly EXIT_FUNC__CHECK_EXIT_CODE=11
readonly EXIT_FUNC__LOG_UNIMPLEMENTED=12
readonly EXIT_FUNC__LOG_ARGUMENT_ERROR_VALUE=13
readonly EXIT_FUNC__LOG_ARG_PROVIDED_ERROR=14
readonly EXIT_FUNC__DO_MOUNT=15
readonly EXIT_FUNC__DO_UMOUNT=16
readonly EXIT_FUNC__IS_MOUNTPOINT=17

# Used to store the output of stderr of various commands
STDERR_LOG_FILE=$(mktemp)
readonly STDERR_LOG_FILE

######################## GLOBALS ########################

# Used in function log()
G_LOG_LEVEL=1

# Variables that hold the results of a function

# Used by function promptUser()
FUNC_RES__PROMPT_USER=""

# Used by function promptUserYesNo()
FUNC_RES__PROMPT_USER_YES_NO=""

# Used by function promptIndex()
FUNC_RES__PROMPT_INDEX=""

######################## FUNCTIONS ########################

# Get a position in this file
#
# PARAMETERS
#   [$1] - The line number
#   [$2] - The column number
#
# RETURN
# The position in this script for the specified line and column.
function getFilePosition() {
  local position="${ANSI_BOLD}$SCRIPT_PATH"

  # add line
  if [ -n "$1" ]; then
    position="$position:$1"
  fi

  # add column
  if [ -n "$2" ]; then
    position="$position:$2"
  fi

  echo -n "$position${ANSI_END}"
}

# Log a an error at the line where a function was called.
#
# PARAMETERS
#   $1  - The index of the function in the execution call stack.
#        See
#          https://www.gnu.org/savannah-checkouts/gnu/bash/manual/bash.html#index-FUNCNAME
#          https://www.gnu.org/savannah-checkouts/gnu/bash/manual/bash.html#index-BASH_005fLINENO
#
#   $2  - Exit code for the script
#   $3  - The message that should be printed
function logErrorAtLine() {
  local line=${BASH_LINENO[$1]}
  local prefix="$(getFilePosition "$line")"
  ((exitCode = $2))

  log 5 $exitCode "$prefix\n$3"
}

# Log an error message indicating that a function has not yet
# been implemented.
#
# EXAMPLE
# function myCoolFunc() {
#   logUnimplemented
# }
function logUnimplemented() {
  local func=${FUNCNAME[1]}

  local msg="The function '$func' was called, but"
  msg="$msg '$func' has not been implemented yet."

  logErrorAtLine 2 $EXIT_FUNC__LOG_UNIMPLEMENTED "$msg"
}

# Log an error message indicating that an argument to a
# function is invalid.
#
# PARAMETERS
#   $1  - The invalid value
#  [$2] - The name of the argument
#  [$3] - A message describing why $1 is invalid
function logArgumentErrorValue() {
  local func=${FUNCNAME[1]}
  local errorMsg="Invalid argument in $func"

  # Append the argument name, if there is one.
  if [ -n "$2" ]; then
    errorMsg="$errorMsg ($2)"
  fi

  # Append the message, if there is one.
  if [ -n "$3" ]; then
    errorMsg="$errorMsg: $3"
  fi

  # Append the invalid value.
  errorMsg="$errorMsg: $1"

  logErrorAtLine 2 $EXIT_FUNC__LOG_ARGUMENT_ERROR_VALUE "$errorMsg"
}

# Log an error message indicating that an argument to the
# script has already been provided.
#
# PARAMETERS
#   $1  - $TRUE if the argument has already been provided, else $FALSE
#   $2  - The name of the argument
#   $3  - The value of the argument
function logArgProvidedError() {
  local hasBeenProvided=$1
  local name=$2
  local value=$3

  # Validate args

  if [ $TRUE != "$(isBoolString "$hasBeenProvided")" ]; then
    logArgumentErrorValue "$hasBeenProvided" "hasBeenProvided" "Must be a bool"
  fi

  if [ -z "$name" ]; then
    logArgumentErrorValue "$name" "name" "Must not be an empty string"
  fi

  # Function implementation

  if [ $FALSE = "$hasBeenProvided" ]; then
    return
  fi

  logErrorAtLine \
    2 \
    $EXIT_FUNC__LOG_ARG_PROVIDED_ERROR \
    "A value for the argument '$name' has already been provided:\n$value"
}

# Print a log message to stdout.
#
# PARAMETERS
#   $1  - The log level of the message
#
# If the message's log level is NOT error
#   $2  - The message that should be printed
#
# If the message's log level is error
#   $2  - Exit code for the script
#   $3  - The message that should be printed
#  [$4] - Extra code to evaluate before exiting
#
# LOG LEVELS
# verbose -> 1
# debug -> 2
# info -> 3
# warning -> 4
# error -> 5
#
# EXAMPLE
# log 3 "My info message"
# log 5 24 "My error message with exit code 24"
# log 5 10 "My error message with exit code 10" "echo 'I will be evaluated by eval'"
function log() {
  if [ "$G_LOG_LEVEL" -gt "$1" ]; then
    return
  fi

  # verbose
  if [ "$1" -eq 1 ]; then
    echo -e "${TEXT_STYLE_VERBOSE}VERBOSE:${ANSI_END} $2"
    return
  fi

  # debug
  if [ "$1" -eq 2 ]; then
    return
  fi

  # info
  if [ "$1" -eq 3 ]; then
    echo -e "${TEXT_STYLE_INFO}INFO:${ANSI_END} $2"
    return
  fi

  # warning
  if [ "$1" -eq 4 ]; then
    echo -e "${TEXT_STYLE_WARNING}WARNING:${ANSI_END} $2"
    return
  fi

  # error
  if [ "$1" -eq 5 ]; then
    echo -e "${TEXT_STYLE_ERROR}ERROR!${ANSI_END}"
    echo -e "$3"
    echo

    eval "$4"

    exit "$2"
  fi
}

# Check if the command in the previous line exited with code 0.
#
# Exits with code $EXIT_FUNC__CHECK_EXIT_CODE, if the previous
# command encountered an error. Also prints the contents of
# $STDERR_LOG_FILE.
function checkExitCode() {
  local code=$?

  if [ 0 -eq $code ]; then
    return
  fi

  local line=${BASH_LINENO[0]}

  # Decrement line by one so that the position points at the
  # command and not at the checkExitCode function.
  ((line--))

  local msg="$(getFilePosition $line)\n"
  msg="${msg}A command exited with the code: $code\n"
  msg="${msg}------------------------------------------\n"
  msg="${msg}STDERR:\n$(cat "$STDERR_LOG_FILE")"

  log 5 $EXIT_FUNC__CHECK_EXIT_CODE "$msg"
}

# Prompt the user with a question and receive some input.
#
# PARAMETERS
#   $1  - The question for the user
#
# RETURN
# The string the user wrote. The string is stored in $FUNC_RES__PROMPT_USER.
# $FUNC_RES__PROMPT_USER is set to "" at the beginning of the function.
function promptUser() {
  # Validate args

  if [ -z "$1" ]; then
    logArgumentErrorValue "$1" "question" "Must not be an empty string"
  fi

  # Function implementation

  FUNC_RES__PROMPT_USER=""
  echo -en "$1"
  read -r FUNC_RES__PROMPT_USER
  echo
}

# Prompt the user with a yes/no question.
#
# PARAMETERS
#   $1  - The question for the user, " [Y/n]: " or " [y/N]: " gets appended.
#  [$2] - The default answer, $TRUE for yes and $FALSE for no. Defaults to $TRUE.
#
# RETURN
# The answer the user gave to the question. $TRUE for yes and $FALSE for no.
# The answer is stored in $FUNC_RES__PROMPT_USER_YES_NO.
# $FUNC_RES__PROMPT_USER_YES_NO is set to "" at the beginning of the function.
function promptUserYesNo() {
  # Validate args

  if [ -z "$1" ]; then
    logArgumentErrorValue "$1" "question" "Must not be an empty string"
  fi

  # Function implementation

  FUNC_RES__PROMPT_USER_YES_NO=""

  local defaultRes
  local msg="$1"

  if [ $FALSE = "$2" ]; then
    msg="$msg [y/N]"
    defaultRes=$FALSE
  else
    msg="$msg [Y/n]"
    defaultRes=$TRUE
  fi

  promptUser "$msg: "

  case $FUNC_RES__PROMPT_USER in
  "y" | "Y") FUNC_RES__PROMPT_USER_YES_NO=$TRUE ;;
  "n" | "N") FUNC_RES__PROMPT_USER_YES_NO=$FALSE ;;
  *) FUNC_RES__PROMPT_USER_YES_NO=$defaultRes ;;
  esac
}

# Prompt the user to provide an index for an array.
#
# PARAMETERS
#   $1  - The length of the array
#   $2  - The question for the user
#
# RETURN
# The index the user provided, always a number between 0 and (($1 - 1)).
# The index is stored in $FUNC_RES__PROMPT_INDEX.
# $FUNC_RES__PROMPT_INDEX is set to "" at the beginning of the function.
function promptIndex() {
  local arrayLength=$1
  local prompt=$2

  # Validate args

  if [ $TRUE != "$(isNumericString "$arrayLength")" ]; then
    logArgumentErrorValue "$arrayLength" "arrayLength" "Must be a numeric string"
  elif [ "$arrayLength" -le 0 ]; then
    logArgumentErrorValue "$arrayLength" "arrayLength" "Must be greater than 0"
  fi

  # Function implementation

  FUNC_RES__PROMPT_INDEX=""

  local index
  while $TRUE; do
    promptUser "$prompt"
    index=$FUNC_RES__PROMPT_USER

    # Tell user he must enter a number not any characters
    if [ $TRUE != "$(isNumericString "$index")" ]; then
      echo -e "${TEXT_STYLE_ERROR}" \
        "You must enter a number" \
        "${ANSI_END}\n"
      continue
    fi

    # Tell user he must pick an index between 0 and arrayLength-1
    if [ "$index" -lt 0 ] || [ "$index" -ge "$arrayLength" ]; then
      if [ 1 -eq "$arrayLength" ]; then
        echo -e "${TEXT_STYLE_ERROR}" \
          "You must enter a valid index (only 0 is valid)" \
          "${ANSI_END}\n"
      else
        echo -e "${TEXT_STYLE_ERROR}" \
          "You must enter a valid index (between 0 and $((arrayLength - 1)))" \
          "${ANSI_END}\n"
      fi
      continue
    fi

    # If index is valid then return
    FUNC_RES__PROMPT_INDEX=$index
    return
  done
}

# Show the user a message and pause the execution of the script
# until the user wants to continue.
#
# PARAMETERS
#  [$1] - A message to show to the user
function pause() {
  local msg
  if [ -z "$1" ]; then
    msg=""
  else
    msg="$1\n"
  fi
  msg="${msg}Press any key to continue..."

  echo -e "$msg"
  read -r
}

# Check if the provided string is a numeric string. A numeric string contains
# only digits from 0 to 9 and and optional single hyphen (-) at index 0. Also
# a numeric string cannot be empty.
#
# PARAMETERS
#   $1  - The string you want to check
#
# RETURN
# $TRUE if $1 is numeric, else $FALSE
function isNumericString() {
  local str=$1

  # If the first character is a minus, then remove it
  if [ "-" = "${str:0:1}" ]; then
    str=${str:1}
  fi

  # Check that str only contains digits
  if expr "$str" : '^[0-9]*$' >/dev/null; then
    echo -n $TRUE
  else
    echo -n $FALSE
  fi
}

# Check if the provided string is a bool string. A bool string must be
# equal to $TRUE or $FALSE.
#
# PARAMETERS
#   $1  - The string you want to check
#
# RETURN
# $TRUE if $1 is a bool, else $FALSE.
function isBoolString() {
  if [ $TRUE = "$1" ] || [ $FALSE = "$1" ]; then
    echo -n $TRUE
  else
    echo -n $FALSE
  fi
}

# Check if the provided path points to a directory and if it does,
# prompt the user if it is okay to delete the directory.
# After that create the directory again.
#
# This function exits the script if the directory could not be deleted
# or could not be created.
#
# PARAMETERS
#   $1  - The path to the directory
#   $2  - The exit code, in case the directory could not be created or deleted
function recreateDirectory() {
  local dirpath=$1
  local exitCode=$2

  # Validate args

  if [ -z "$dirpath" ]; then
    logArgumentErrorValue \
      "$dirpath" \
      "dirpath" \
      "Must not be an empty string"
  fi

  if [ "$TRUE" != "$(isNumericString "$exitCode")" ]; then
    logArgumentErrorValue \
      "$exitCode" \
      "exitCode" \
      "Must not be an empty string"
  elif [ "$exitCode" -lt 0 ]; then
    logArgumentErrorValue \
      "$exitCode" \
      "exitCode" \
      "Must be a positive integer"
  fi

  # Function implementation

  # Check if $dirpath already exists as a file or as a directory
  # and delete it if it does
  if [ -e "$dirpath" ]; then
    local prompt="The"
    if [ -d "$dirpath" ]; then
      prompt="${prompt} directory"
    else
      prompt="${prompt} file"
    fi
    prompt="${prompt} '$dirpath' already exist, can it be deleted ?"

    # Ask if $dirpath can be deleted
    promptUserYesNo "$prompt"
    if [ $TRUE != $FUNC_RES__PROMPT_USER_YES_NO ]; then
      log \
        5 \
        "$exitCode" \
        "Aborting, because '$dirpath' already exists but cannot be deleted."
    fi

    # Delete $dirpath
    local rmStdout=$(rm -rf "$dirpath" 2>&1)
    local rmCode=$?
    if [ 0 -ne $rmCode ]; then
      local msg="Aborting, because '$dirpath' could not be deleted."
      if [ -n "$rmStdout" ]; then
        msg="${msg}\n$rmStdout"
      fi

      log 5 "$exitCode" "$msg"
    fi
  fi

  createDirectory "$dirpath" "$exitCode"
}

# Create a new directory at
#
# This function exits the script if the directory could not
# be created.
#
# PARAMETERS
#   $1  - The path to the directory
#   $2  - The exit code, in case the directory could not be created
#  [$3] - $TRUE if the parent directories in the provided path should
#         also be created, else $FALSE. Defaults to $FALSE.
function createDirectory() {
  local dirpath=$1
  local exitCode=$2
  local createParents=$3

  # Set default values for args

  if [ -z "$createParents" ]; then
    createParents=$FALSE
  fi

  # Validate args

  if [ -z "$dirpath" ]; then
    logArgumentErrorValue \
      "$dirpath" \
      "dirpath" \
      "Must not be an empty string"
  fi

  if [ "$TRUE" != "$(isNumericString "$exitCode")" ]; then
    logArgumentErrorValue \
      "$exitCode" \
      "exitCode" \
      "Must not be an empty string"
  elif [ "$exitCode" -lt 0 ]; then
    logArgumentErrorValue \
      "$exitCode" \
      "exitCode" \
      "Must be a positive integer"
  fi

  if [ $TRUE != "$(isBoolString "$createParents")" ]; then
    logArgumentErrorValue "$createParents" "createParents" "Must be a bool"
  fi

  # Function implementation

  local mkdirArgs=""
  if [ $TRUE = "$createParents" ]; then
    mkdirArgs="-p"
  fi
  local mkdirStdout=$(mkdir $mkdirArgs "$dirpath" 2>&1)
  local mkdirCode=$?

  if [ 0 -eq $mkdirCode ]; then
    return
  fi

  local msg="Aborting, because '$dirpath' could not be created.\n"
  msg="${msg}mkdir exit code: $mkdirCode"
  if [ -n "$mkdirStdout" ]; then
    msg="${msg}\n$mkdirStdout"
  fi

  log 5 "$exitCode" "$msg"
}

# Mount a target at a provided mountpoint.
#
# This function exits the script if the target could not
# be mounted
#
# PARAMETERS
#   $1  - The target you want to mount
#   $2  - The mountpoint for the target
#  [$3] - Additional arguments for the "mount" command
function doMount() {
  local target=$1
  local mountpoint=$2
  local mountArgs=$3

  recreateDirectory "$mountpoint" $EXIT_FUNC__DO_MOUNT

  # Check if target exists
  if ! [ -f "$target" ]; then
    log \
      5 \
      $EXIT_FUNC__DO_MOUNT \
      "The target you want to mount does not exist\n$target"
  fi

  # $mountArgs cannot be enclosed in "", because calling
  # mount like this:
  # mount "-o loop"
  # causes errors.
  # shellcheck disable=SC2086
  local mountStdout=$(mount $mountArgs "$target" "$mountpoint" 2>&1)
  local mountCode=$?

  # Return if mount succeeded
  if [ 0 -eq $mountCode ]; then
    return
  fi

  local msg="Failed to mount '$target' at '$mountpoint'.\n"
  msg="${msg}mount exit code: $mountCode"
  if [ -n "$mountStdout" ]; then
    msg="${msg}\n$mountStdout"
  fi

  log 5 $EXIT_FUNC__DO_MOUNT "$msg"
}

# Unmount the target at the provided mountpoint.
#
# This function exits the script if the target could not
# be mounted
#
# PARAMETERS
#   $1  - The mountpoint of the target
function doUmount() {
  local mountpoint=$1

  if ! [ -e "$mountpoint" ]; then
    log 1 "The provided mountpoint does not exist:\n$mountpoint"
    return
  fi

  local umountStdout=$(umount "$mountpoint")
  local umountCode=$?

  # Return if umount succeeded
  if [ 0 -eq $umountCode ]; then
    return
  fi

  local msg="Failed to unmount '$mountpoint'.\n"
  msg="${msg}umount exit code: $umountCode"
  if [ -n "$umountStdout" ]; then
    msg="${msg}\n$umountStdout"
  fi

  log 5 $EXIT_FUNC__DO_UMOUNT "$msg"
}

# Check if the provided filepath is a mountpoint.
#
# PARAMETERS
#   $1  - The filepath of an assumed mountpoint
#
# RETURN
# $TRUE if $1 is a mountpoint, else $FALSE.
function isMountpoint() {
  local filepath=$1

  if ! [ -e "$filepath" ]; then
    log \
      5 \
      $EXIT_FUNC__IS_MOUNTPOINT \
      "The provided filepath does not exist:\n$filepath"
  fi

  local mountpointStdout=$(mountpoint -q "$mountpoint")
  local mountpointCode=$?

  # For all exit codes of "mountpoint", see "man mountpoint"
  case $mountpointCode in
  # success; the directory is a mountpoint
  0)
    echo -n $TRUE
    ;;

  # failure; incorrect invocation, permissions or system error
  1)
    local msg="Incorrect invocation of 'mountpoint', or not enough permissions\n"
    msg="${msg}$mountpointStdout"
    logErrorAtLine 1 "$msg"
    ;;

  # failure; the directory is not a mountpoint
  32)
    echo -n $FALSE
    ;;

  *)
    logErrorAtLine 1 "Unrecognized exit code for 'mountpoint': $mountpointCode"
    ;;
  esac
}

# Print info at the start of the script.
function info() {
  echo "$PROGNAME v$VERSION"
  echo "Author: $AUTHOR"
  echo "Report Email: $REPORT_EMAIL"
  echo
}

# Print the help message for this script and exit
# with code $EXIT_SUCCESS.
function help() {
  logUnimplemented

  exit $EXIT_SUCCESS
}

######################## COMMANDS ########################

function commandMount() {
  # If no argument for $imgPath was passed then prompt the user for a path
  if [ -z "$imgPath" ]; then
    promptUser "Enter the path to the image you want to mount:\n"
    imgPath=$FUNC_RES__PROMPT_USER
  fi

  # Check if $imgPath exists
  if ! [ -f "$imgPath" ]; then
    log 5 $EXIT_IMG_PATH_DOES_NOT_EXIST "The file '$imgPath' does not exist."
  fi

  # If $imgPath is a squash image, then we need to mount
  # the squash image, before we can use the partition command.
  if [ $TRUE = $isSquashImg ]; then
    local squashImgPath=$imgPath
    local squashImgMountpoint="$mountpoint/squash"

    # Check that $squashImgPath is not already mounted
    if [ $TRUE = "$(isMountpoint "$squashImgMountpoint")" ]; then
      log \
        5 \
        $EXIT_SQUASH_IMG_ALREADY_MOUNTED \
        "The squash image '$squashImgPath' is already mounted at '$squashImgMountpoint'"
    fi

    # Assure that the directory $mountpoint does not exist
    recreateDirectory "$mountpoint" $EXIT_FAILED_TO_CREATE_MOUNTPOINT

    log 3 "Now mounting the squash image '$squashImgPath' at '$squashImgMountpoint'..."
    doMount "$squashImgPath" "$squashImgMountpoint"
    log 3 "Finished!\n"

    local files=$(find "$squashImgMountpoint" -mindepth 1 -print0 | xargs -0 -n1 echo)
    local filesLength=${#files[@]}

    # If there is no file in the mountpoint, then there is no
    # value for $imgPath with is invalid.
    if [ 0 -eq "$filesLength" ]; then
      msg="After mounting '$squashImgPath' the directory "
      msg="${msg}'$squashImgMountpoint' is still empty and "
      msg="${msg}does not contain an image file."
      log 5 $EXIT_NO_IMG_IN_SQUASH "$msg"
    fi

    # If the mounted squash image only contains a single file,
    # then we set the path of $imgPath to that file. Otherwise
    # we prompt the user to select a file.
    if [ 1 -eq "$filesLength" ]; then
      imgPath=${files[0]}
    else
      msg="Because '$squashImgMountpoint' contains multiple files, you "
      msg="${msg}need to choose which of those files is the image "
      msg="${msg}file that contains the partition you want to mount."
      pause "$msg"
      for ((i = 0; i < filesLength; i++)); do
        echo -e "$ANSI_BOLD$i)$ANSI_END ${files[i]}"
      done
      echo

      promptIndex "$filesLength" "Please choose a file: "
      imgPath=${files[$FUNC_RES__PROMPT_INDEX]}
    fi
  else
    # Assure that the directory $mountpoint does not exist
    recreateDirectory "$mountpoint" $EXIT_FAILED_TO_CREATE_MOUNTPOINT
  fi

  # Retrieve the partition table of "$imgPath"
  #
  # Without the "--machine" option, the output of parted looks like this
  # ```
  # Model:  (file)
  # Disk /mnt/tester_nona/nvme0n1.image: 500107862016B
  # Sector size (logical/physical): 512B/512B
  # Partition Table: gpt
  # Disk Flags:
  #
  # Number  Start       End            Size           File system  Name                  Flags
  #  1      1048576B    537919487B     536870912B     fat32        EFI System Partition  boot, esp
  #  2      537919488B  500106788863B  499568869376B  ext4
  # ```
  #
  # with the "--machine" option it looks like this
  # ```
  # BYT;
  # /mnt/tester_nona/nvme0n1.image:500107862016B:file:512:512:gpt::;
  # 1:1048576B:537919487B:536870912B:fat32:EFI System Partition:boot, esp;
  # 2:537919488B:500106788863B:499568869376B:ext4::;
  # ```
  #
  # Here is an explanation of the machine output
  #
  # 1. `BYT;`:
  #    This indicates the units used for the values in the output.
  #    In this case, it's using bytes (B) as the unit of measurement.
  #
  # 2. `/mnt/tester_nona/nvme0n1.image:500107862016B:file:512:512:gpt::;`:
  #    This part of the output provides information about the disk itself.
  #    It includes the path to the disk image file (`/mnt/tester_nona/nvme0n1.image`),
  #    the size of the disk in bytes (500107862016B), the type of the source (in this
  #    case, it's a file), and some additional information about the disk. The `gpt`
  #    indicates that it's using the GPT (GUID Partition Table) partitioning scheme.
  #
  # 3. `1:1048576B:537919487B:536870912B:fat32:EFI System Partition:boot, esp;`:
  #      This part of the output represents the first partition on the disk. It includes
  #      the following information:
  #
  #    - `1`: The partition number.
  #    - `1048576B`: The start point of the partition in bytes.
  #    - `537919487B`: The end point of the partition in bytes.
  #    - `536870912B`: The size of the partition in bytes.
  #    - `fat32`: The filesystem type (FAT32 in this case).
  #    - `EFI System Partition`: A label or name for the partition.
  #    - `boot, esp`: Additional flags indicating that it's a bootable partition and
  #                   an ESP (EFI System Partition).
  #
  # 4. `2:537919488B:500106788863B:499568869376B:ext4::;`:
  #    This part of the output represents the second partition on the disk.
  #    It includes similar information to the first partition, such as the
  #    partition number, start and end points, size, and filesystem type.
  #    In this case, it's an `ext4` filesystem.
  #
  # In summary, this output is detailing the partitions on a disk, including
  # their partition numbers, sizes, filesystem types, labels, and flags. It
  # also provides information about the disk itself, such as its size and
  # partitioning scheme.
  local partitionTable=$(parted --machine --script "$imgPath" \
    unit B \
    print \
    quit 2>"$STDERR_LOG_FILE")
  checkExitCode

  local partitionTableLines
  mapfile -d ";" -t partitionTableLines < <(echo -n "$partitionTable" | tr -d "\n")
  local partitionTableLinesLength=${#partitionTableLines[@]}

  # If $partitionTableLinesLength is equal to, or less than 2,
  # then there are no partitions
  if [ "$partitionTableLinesLength" -le 2 ]; then
    log 5 $EXIT_NO_PARTITIONS "The image '$imgPath' does not contain any partitions."
  fi

  # Create a prompt message for the user, so that they can choose
  # the partition they want to mount
  #
  # Number:Start:End:Size:File system:Name:Flags
  local indexedPartitions="${ANSI_BOLD}INDEX${ANSI_END}:"
  indexedPartitions="${indexedPartitions}Partition number:Start:End:Size:File system:Name:Flags\n"
  for ((i = 2; i < partitionTableLinesLength; i++)); do
    # A line looks like this
    # 1:1048576B:537919487B:536870912B:fat32:EFI System Partition:boot, esp

    ((index = i - 2))
    indexedPartitions="$indexedPartitions${ANSI_BOLD}$index)${ANSI_END}:"
    indexedPartitions="$indexedPartitions${partitionTableLines[i]}\n"
  done

  # Prompt user for the index of the partition he wants to mount
  echo -en "$indexedPartitions" | column -t -s ":"
  echo
  promptIndex \
    $((partitionTableLinesLength - 2)) \
    "Enter the index of the partition you want to mount: "
  local chosenPartitionIndex=$FUNC_RES__PROMPT_INDEX

  # A line looks like this
  # Number:Start:End:Size:File system:Name:Flags
  local chosenPartitionInfo
  mapfile -d ":" -t chosenPartitionInfo < <(echo -n "${partitionTableLines[((chosenPartitionIndex + 2))]}")

  # Extract info from $chosenPartitionInfo
  local partitionNumber=${chosenPartitionInfo[0]}
  local startOffsetBytes=${chosenPartitionInfo[1]}
  # local endOffsetBytes=${chosenPartitionInfo[2]}
  # local sizeInBytes=${chosenPartitionInfo[3]}
  # local filesystem=${chosenPartitionInfo[4]}
  # local name=${chosenPartitionInfo[5]}
  # local flags=${chosenPartitionInfo[6]}

  # Remove the unit from the offset (e.g.: "123456B" -> "123456")
  startOffsetBytes="$(echo -n "$startOffsetBytes" | tr -c -d "0-9")"

  # Create the mountpoint for the partition
  local partitionMountpoint="$mountpoint/$(basename "$imgPath").partition-$partitionNumber"

  # Check that $partitionMountpoint is not already a used mountpoint
  if [ $TRUE = "$(isMountpoint "$partitionMountpoint")" ]; then
    log \
      5 \
      $EXIT_PARTITION_ALREADY_MOUNTED \
      "The selected partition is already mounted at '$partitionMountpoint'"
  fi

  # Mount the partition at the provided offset
  log 3 "Now mounting the $chosenPartitionIndex.partition at '$partitionMountpoint'..."
  doMount "$imgPath" "$partitionMountpoint" "-o loop,ro,offset=$startOffsetBytes"
  log 3 "Finished!\n"
}

function commandUmount() {
  if ! [ -e "$mountpoint" ]; then
    log \
      3 \
      "Nothing to unmount because the mountpoint '$mountpoint' does not exist."
    exit $EXIT_SUCCESS
  fi

  local partitionMountpoints=$(
    find "$mountpoint" -mindepth 1 -maxdepth 2 -name "*.partition-*" -print0 |
      xargs -0 -n1 echo
  )

  # Unmount all partitions
  for item in "${partitionMountpoints[@]}"; do
    if [ $TRUE = "$(isMountpoint "$item")" ]; then
      log 3 "Unmounting a partition at '$item'..."
      doUmount "$item"
      log 3 "Finished!\n"
    fi
  done

  # Unmount the squash image, if it is one
  if [ $TRUE = $isSquashImg ]; then
    local squashImgMountpoint="$mountpoint/squash"
    if [ $TRUE = "$(isMountpoint "$squashImgMountpoint")" ]; then
      log 3 "Now unmounting the squash image '$imgPath' at '$squashImgMountpoint'..."
      doUmount "$squashImgMountpoint"
      log 3 "Finished!\n"
    fi
  fi

  promptUserYesNo "Do you want to delete '$mountpoint' ?"
  if [ $TRUE = $FUNC_RES__PROMPT_USER_YES_NO ]; then
    rm -rf "$mountpoint"
  fi
}

######################## SCRIPT ########################

# The path to the image file. Can be either a squash image or
# a normal image file.
imgPath=""

# $TRUE if $imgPath points to a squash image, else $FALSE.
# A squash image is an image that was create using mksquashfs
isSquashImg=$FALSE

# $TRUE if the chosen partition of image should be mounted and
# $FALSE if it should be unmounted.
shouldMount=""

mountpoint=""

# Parse all command line args and exit with $EXIT_UNKNOWN_ARG
# if an unrecognized arg is encountered.
args=("$@")
declare -r argsLength=${#args[@]}

parsedArgImgPath=$FALSE
parsedArgIsSquash=$FALSE
parsedArgShouldMount=$FALSE
parsedArgMountpoint=$FALSE
for ((i = 0; i < argsLength; i++)); do
  arg=${args[i]}

  case $arg in
  "-h" | "--help")
    help
    ;;

  "--image-path")
    logArgProvidedError $parsedArgImgPath "--image-path" "$imgPath"

    ((i += 1))
    if [ $i -ge "$argsLength" ]; then
      log 5 $EXIT_NO_VALUE_FOR_ARG "No value for --image-path was provided."
    fi

    imgPath=${args[i]}
    parsedArgImgPath=$TRUE
    ;;

  "--squash")
    logArgProvidedError $parsedArgIsSquash "--squash" "$isSquashImg"
    isSquashImg=$TRUE
    parsedArgIsSquash=$TRUE
    ;;

  "--mount")
    logArgProvidedError $parsedArgShouldMount "--mount" "$shouldMount"
    shouldMount=$TRUE
    parsedArgShouldMount=$TRUE
    ;;

  "--umount")
    logArgProvidedError $parsedArgShouldMount "--umount" "$shouldMount"
    shouldMount=$FALSE
    parsedArgShouldMount=$TRUE
    ;;

  "--mountpoint")
    logArgProvidedError $parsedArgMountpoint "--mountpoint" "$mountpoint"

    ((i += 1))
    if [ $i -ge "$argsLength" ]; then
      log 5 $EXIT_NO_VALUE_FOR_ARG "No value for --mountpoint was provided."
    fi

    mountpoint=${args[i]}
    parsedArgMountpoint=$TRUE
    ;;

  *)
    log 5 $EXIT_UNKNOWN_ARG "Unknown argument: $arg"
    ;;
  esac
done

info

# If no argument for $mountpoint was passed then prompt the user for a path
if [ -z "$mountpoint" ]; then
  promptUser "Enter the path to the mountpoint:\n"
  mountpoint=$FUNC_RES__PROMPT_USER
fi

# If neither the --mount or --umount argument was provided, then
# prompt the user what he wants to do.
if [ $TRUE != $parsedArgShouldMount ]; then
  msg="Do you want to mount an image, if not then "
  msg="${msg}the provided mountpoint will be unmounted ?"
  promptUserYesNo "$msg"
  shouldMount=$FUNC_RES__PROMPT_USER_YES_NO
fi

if [ $TRUE = $shouldMount ]; then
  commandMount
else
  commandUmount
fi

exit $EXIT_SUCCESS
