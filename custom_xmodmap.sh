#! /bin/bash

# Apply custom keyboard configurations using xmodmap, if there are any
# and if xmodmap is available.

readonly USERXMODMAP="$HOME/.Xmodmap"

if which xmodmap >/dev/null 2>&1 && [ -f "$USERXMODMAP" ]; then
	# Wait before executing xmodmap, so that setxkbmap does not override
	# the custom changes.
  sleep 5

  xmodmap "$USERXMODMAP"
fi
