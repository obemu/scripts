#! /bin/bash

######################## Constants ########################

readonly REPORT_EMAIL="emanueloberholzer400@gmail.com"

readonly SCRIPT="/home/emanuel/Projects/DartProjects/copy_to_adb_device.dart"

readonly VERSION="1.1.0"

# See https://en.wikipedia.org/wiki/ANSI_escape_code#Colors
readonly TEXT_STYLE_END="\033[m"
readonly TEXT_STYLE_ERROR="\033[1;31m"
readonly TEXT_STYLE_INFO="\033[1;4;33m"

######################## Function definitions ########################

function error() {
  echo -e "${TEXT_STYLE_ERROR}ERROR!${TEXT_STYLE_END}"
  echo -e "$2"
  echo

  exit "$1"
}

function version() {
  echo "cp-audio-to-adb-device v$VERSION"
}

function help() {
  version

  echo "Usage: cp-audio-to-adb-device LOCAL_DIR"
  echo "Copy the contents of LOCAL_DIR to the audio folder of an adb device."
  echo "Example: cp-audio-to-adb-device ~/Music"
  echo

  echo "Report bugs to: $REPORT_EMAIL"

  exit 0
}

######################## Script ########################

if [ -z "$1" ] || [ "-h" = "$1" ] || [ "--help" = "$1" ]; then
  help
fi

if ! [ -e $SCRIPT ]; then
  error 1 "The dart script \"$SCRIPT\" does not exist!"
fi

localDirectory=$1

if ! [ -d "$localDirectory" ]; then
  error 2 "The directory \"$localDirectory\" does not exist!"
fi

echo "To which device do you want to copy your audio files?"
echo "1) Samsung S6"
echo "2) Samsung S7"
read -r chosen
echo

remoteTargetDir=""

case $chosen in
1) remoteTargetDir="/sdcard/Music" ;;
2) remoteTargetDir="/storage/9C33-6BBD/_Music" ;;
esac

dart $SCRIPT $localDirectory $remoteTargetDir
exitCode=$(echo $?)

echo

exit "$exitCode"

# Code from v1.0.0
#
# err=""
# out=""
# exitCode=""
# {
#   IFS= read -rd '' err
#   IFS= read -rd '' out
#   IFS= read -rd '' exitCode
# } < <(
#   { out=$(dart $SCRIPT $localDirectory $remoteTargetDir); } 2>&1
#   printf '\0%s' "$out" "$?"
# )

# if ([ 0 -ne $exitCode ]); then
#   if ([ -z $out ]); then
#     echo $out
#   fi

#   error "$exitCode" "$err"
# fi

# exit $exitCode
