#! /bin/bash

mapfile -t devices < <(adb devices)
readonly devicesLength=${#devices[@]}

# If there are no attached devices then the length of the devices array is 2
# because the element at the first index is always "List of devices attached"
# and the element at the last index is always an empty line.
if [ 2 -eq "$devicesLength" ]; then
  echo "No devices attached"
  exit 1
fi

################################################
# See https://android.stackexchange.com/a/154328
################################################
# screenrecord is an internal Android executable that dumps screen to a
# file, and ffplay from ffmpeg happens to be able to play an H.264 encoded
# stream from stdin
#
# -probesize 32 -framerate 60 -sync video reduce the delay between the image
# on the phone and the image on the disktop
#
# we use adb exec-out instead of adb shell because shell might mess up some
# control characters due to being a shell
# (https://stackoverflow.com/questions/13578416/read-binary-stdout-data-from-adb-shell/31401447#31401447)
#
# See also: https://stackoverflow.com/questions/31472962
adb exec-out screenrecord --output-format=h264 - | ffplay -framerate 60 -probesize 32 -sync video  -

exit 0
